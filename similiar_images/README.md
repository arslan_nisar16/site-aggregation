Script uses selenium to scrape google image search in order to find similar products by its image. 

# Dependency

- Firefox
- Selenium
- xvfb
- pyvirtualdisplay


It works on products from active categories. Scrape in following order: start
from products that never were crawled, than latest crawl first

#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
import os
import time
import datetime
import urllib
import urlparse
import random
import atexit

from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import NoSuchElementException

from pyvirtualdisplay import Display

import logging

logging.basicConfig(filename='google_image.log',
    format='%(asctime)s %(message)s')

logger = logging.getLogger() #'google_images')
logger.setLevel(logging.INFO)


# setup django models

#os.environ['DJANGO_SETTINGS_MODULE'] = 'dj_shopcrawler.settings'
sys.path.append(os.path.join(os.path.abspath(os.path.dirname(__file__)),
        os.pardir))
import django
django.setup()

from products.models import Category, Product



GOOGLE_URL='https://www.google.ru/search?q={query}&tbm=isch'

ERROR_FOLDER = 'error_pages'
MAX_ERRORS = 25

MIN_SLEEP = 5
MAX_SLEEP = 15


def random_sleep(min_sleep=MIN_SLEEP, max_sleep=MAX_SLEEP):
    """Sleep for random time between minsleep and max_sleep"""
    time.sleep(random.randrange(min_sleep, max_sleep) + random.random())


def req_sleep():
    random_sleep(1, 3)


def action_sleep():
    random_sleep(0, 2)


def search_sleep():
    random_sleep()


def scrape_image_url(image_url, site_only=None):
    global browser
    br = browser
    logger.info("scraping image url {}".format(image_url))
    if not image_url:
        logger.info("Got none image, not scraping")
        return
    site_query = ''
    if site_only:
        site_query = 'site:{}'.format(site_only)
    # load google images search
    url = GOOGLE_URL.format(query=site_query)
    logger.info("url {}".format(url))
    br.get(url)
    action_sleep()

    # load input for image search
    img_btn = WebDriverWait(browser, 10).until(
            EC.presence_of_element_located((By.ID, 'gs_st0'))
            )
    img_btn.click()
    action_sleep()
    image_inp = br.find_element_by_id('qbui')
    image_inp.click()
    image_inp.send_keys(image_url)
    action_sleep()
    image_inp.submit()
    action_sleep()

    # filter by site
    if site_query:
        search_input = br.find_element_by_id('lst-ib')
        search_input.click()
        search_input.clear()
        action_sleep()
        search_input.send_keys(site_query)
        action_sleep()
        search_input.submit()

    result_urls = []
    while True:
        for link in br.find_elements_by_xpath('//div[@class="g"]/.//h3/a'):
            #logger.info('link element %s' % link)
            href = link.get_attribute('href')
            parsed = urlparse.urlparse(href)
            if 'google' not in parsed.hostname:
                logger.info('href %s' % href)
                result_urls.append(href)
            else:
                params = urlparse.parse_qs(parsed.query)
                logger.info('params %s' % params)
                if params.get('url'):
                    result_urls.append(params['url'])
        try:
            next_page = br.find_element_by_id('pnnext')
            next_page.click()
            req_sleep()
        except NoSuchElementException:
            break
    return result_urls


def init_browser():
    global display, browser
    display = Display(visible=0, size=(1366, 768))
    display.start()
    browser = webdriver.Firefox()
    return browser, display


@atexit.register
def clean_browser():
    global display, browser
    try:
        browser.close()
    except Exception:
        pass
    display.stop()


def add_similar_products(product, urls):
    products = {product}
    for url in urls:
        try:
            new_product = Product.objects.get(url=url)
        except Product.DoesNotExist:
            continue
        for prod in products:
            prod.similar_image_products.add(new_product)
        products.add(new_product)
    if len(products) > 1:
        for product in products:
            product.update_similar_image_products_num()
            product.save()
    logger.info("Found in db and added as similar {} products".format(
        len(products)-1))


def save_state(fname):
    if not os.path.exists(ERROR_FOLDER):
        os.mkdir(ERROR_FOLDER)
    fname = urllib.quote(fname, safe='')
    path = os.path.join(ERROR_FOLDER, fname)
    image_path = os.path.join(ERROR_FOLDER, fname + '.png')
    browser.save_screenshot(image_path)
    with open(path, 'wb') as fp:
        fp.write(browser.page_source.encode('utf-8'))


def main():
    init_browser()
    errors = 0
    for category in Category.objects.filter(active=True):
        logger.info("Processing category {}".format(category))
        site = category.website.base_url
        # sort by last scrape
        for pr in category.products.extra(
                select={'null_start': 'last_google_image_scraped_time is null'},
                order_by=['-null_start', 'last_google_image_scraped_time']
                ):
            logger.info("Processing product {} last scrape is {}".format(pr,
                pr.last_google_image_scraped_time))
            image = pr.get_real_image_or_none()
            if not image:
                logger.info("No image for the product. Skipping")
                continue

            try:
                similar_urls = scrape_image_url(image, site)
            except Exception:
                exc_time = time.time()
                logger.exception(
                    "Got exception while scraping url {} at {}".format(
                        browser.current_url, exc_time
                    )
                )
                save_state(u'{}_{}.html'.format(browser.current_url, exc_time))
                errors += 1
                if errors >= MAX_ERRORS:
                    logger.critical("Got {} errors. Exiting".format(errors))
                    sys.exit()
                continue
            logger.info("Got {} for product {}".format(len(similar_urls), pr))
            pr.last_google_image_scraped_time = datetime.datetime.utcnow()
            add_similar_products(pr, similar_urls)
            pr.save()


if __name__ == "__main__":
    main()

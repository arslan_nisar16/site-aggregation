from django.test import TestCase

from selenium.webdriver.firefox.webdriver import WebDriver
from pyvirtualdisplay.display import Display

#import ipdb

from google_images import init_browser, scrape_image_url, main

from products.factories import (
    #CategoryFactory,
    ProductFactory,
)
from products.models import Category, Product

IMAGE_URL_1 = 'http://www.linuxmint.com/img/logo.png'
global browser, display

#import ipdb


class GoogleImagesTestCase(TestCase):

    def tearDown(self):
        Category.objects.all().delete()
        Product.objects.all().delete()

    def test_init_browser(self):
        """ Tests that 'browser' and 'display' are created and accesible as global variables """
        browser, display = init_browser()
        self.assertIsInstance(browser, WebDriver)
        self.assertIsInstance(display, Display)

    def test_google_images(self):
        """ """
        #category = CategoryFactory.create()
        product = ProductFactory(
            category__website__title='tokopedia',
            url='https://www.tokopedia.com/blazetrilogy/xbox-360a-controller-for-windows',
            imagePath='https://ecs12.tokopedia.net/newimg/cache/300/product-1/2012/5/29/87/pic_87_5543f2d6-707f-11de-9618-0015178e1aa7.jpg'
        )
        browser, display = init_browser()
        main()
        # TODO added asserts below

    def test_scrape_image_url(self):
        """ Tests scrape_image_url function which must return a list of websites URLs """
        browser, display = init_browser()
        website_list = scrape_image_url(IMAGE_URL_1)
        self.assertIsNot(website_list, [])

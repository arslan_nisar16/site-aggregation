## Synopsis

The project **site-aggregation** (a.k.a, ShowCrawler) crawls several e-comerce sites like bukalapak.com and shows the results in [goodobtain.com](http://goodobtain.com/)
The system uses [Scrapy](http://scrapy.org/) for crawling and [Django](https://www.djangoproject.com/) for displaying the results.
goodobtain.com is deployed under */home/goodobtain/site-aggregation*
 

- ### [Installation](INSTALL.md)
- ### [Installation on DigitalOcean‎ with serverpilot](do_serverpilot_install.md)
- ### [Crawling](CRAWLING.md)

## Count similar products

### Count using google image search

similiar_images/google_images.py


### Count using NLP



# Display crawling data

## Sales rate algorithm

    x(i) - number of sales from i-th crawl
    k(i) - sales rate until i-th crawl
    t(i) - time of i-th crawl(hour)

Steps:

1. calculate sales per hour value:

    x(i) - x(i-1) / (t(i) - t(i-1))

2. (Currently disabled) normalise sales per hour for a product that way it is not dependent on sale volume:

    xn(i) = x(i) / max(x(1), x(2), ... x(i))

3. apply [moving average](https://en.wikipedia.org/wiki/Moving_average) for
xn(i) series with window 4 to smooth fluctuations

4. calculate average current sales rate for whole period:

    k(i) = 0.5 * xn(i) + 0.5k(i-1)


## list all crawl results
/product
This is the main view of the site.

## Download all data from database
Method: When staff request to downlad all data, we launch a async job to celery, then celery will dump all the data in db
to /tmp/data_xxxx.csv, at the same time, at the same time, there will redirect to download_state page. 
In the download_state function, we will check the result of celery job, if the job is finished, then will send 
the /tmp/data_xxxx.csv; if not, will sleep some seconds, and again, will redirect to download_state page and 
check again.

Celery job is in /var/www/shop_crawler/apps/products/tasks.py

# Development

## Modules 

The /goodobtain folder contains django code and the /scrapy_shopcrawler folder contains the scrapy code.

## Database 
Database login credential is not shared with this codebase.

## Fabric commands

install Fabric:

    sudo apt-get install fabric


Available commands:

    fab -l

    coverage        Runs the test suite for the specified applications - currently unavailable
    deploy          Deploy changes to production server
    makemigrations  Create new migrations and migrate
    manage          Run django management commands
    migrate         Update the database
    run             Run dev server
    runserver       Run dev server
    shell           django shell
    test            Run django apps' tests.

## Periodical task

Edit site-aggregation/conf/crontab
and then run
crontab $HOME/site-aggregation/conf/crontab

Do not run shell crontab directly.

### Backup database
Database backup will run every Wednesday and Saturday at 1pm.
In case you wanted to run the db backup manually, run
  python manage.py dbbackup.
Say you wanted to backup the database on Monday, you can run that command to backup the database. The .psql file will be at db_backup directory.

### Run crawler
When we restart the crawler, the crawler start from the first product (start from the scratch), instead of from the last product crawled.

## Models
Two models are mainly used: Product and Metrics. Each product object corresponds to one product on the e-commerce site. Each product has a unique URL. We crawl products periodically and hence may crawl each product multiple times. Each time, the crawling result is saved as a Metrics object that records the important properties of this product like price, sales and page view. By compare the data of different Metrics object of the product, we know the change of product's property.

## Python coding standards

Please follow Python coding standards and Django conventions:

* [PEP 0008 -- Style Guide for Python Code](https://www.python.org/dev/peps/pep-0008/)
* [Google Python Style Guide](https://google.github.io/styleguide/pyguide.html)
* [Python Anti-Patterns](http://docs.quantifiedcode.com/python-code-patterns/)
* [Django Coding style](https://docs.djangoproject.com/en/1.9/internals/contributing/writing-code/coding-style/)

# Terminology 

 - **IBC** - image-based classification

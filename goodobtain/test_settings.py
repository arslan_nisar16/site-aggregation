# Testing settings
from . settings import *
import logging

logging.disable(logging.CRITICAL)

TEST_RUNNER = 'django_nose.NoseTestSuiteRunner'
#TEST_RUNNER = 'django_coverage.coverage_runner.CoverageRunner'
DEBUG = False
TEMPLATE_DEBUG = DEBUG


"""
# execute following commands to create test_goodobtain role

sudo -u postgres psql
postgres=# create role test_goodobtain with encrypted password 'test_goodobtain' superuser createdb login;
"""

INSTALLED_APPS.append('django_nose')

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'TEST_NAME': 'test.db',
    }
}

PASSWORD_HASHERS = (
    'django.contrib.auth.hashers.MD5PasswordHasher',
)

#MIDDLEWARE_CLASSES = (
#    'django.contrib.sessions.middleware.SessionMiddleware',
#    'django.middleware.common.CommonMiddleware',
#    'django.middleware.csrf.CsrfViewMiddleware',
#    'django.contrib.auth.middleware.AuthenticationMiddleware',
#    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
#    'django.contrib.messages.middleware.MessageMiddleware',
#)

ALLOWED_HOSTS = ['localhost', '*.localhost']

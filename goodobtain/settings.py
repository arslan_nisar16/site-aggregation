from __future__ import absolute_import
import os
import sys

BASE_DIR = os.path.dirname(os.path.dirname(__file__))

SCRAPY_DIR = os.path.join(BASE_DIR, 'scrapy_shopcrawler')
APPS_DIR = os.path.join(BASE_DIR, 'apps')
LIB_DIR = os.path.join(BASE_DIR, 'lib')
sys.path.insert(0, APPS_DIR)
sys.path.insert(0, LIB_DIR)

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'qzfe^9)_=9np3u6^b$5lc12acgo7q&eg9)pqdi82r-u-vyrsn2'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True
TEMPLATE_DEBUG = DEBUG

ALLOWED_HOSTS = ['*']
# ALLOWED_HOSTS = ["goodobtain.com"]

# Application definition

INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',

    'core',
    'users',
    'products',
    'crawler',

    # third-party apps
    'crispy_forms',
    'captcha',
    'dbbackup',
    'djcelery',
    'bootstrap3',
    'kombu.transport.django',
]


MIDDLEWARE_CLASSES = [
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.locale.LocaleMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]

AUTH_USER_MODEL = 'users.User'
ROOT_URLCONF = 'goodobtain.urls'

WSGI_APPLICATION = 'goodobtain.wsgi.application'

LOCALE_PATHS = (
    os.path.join(BASE_DIR, 'locale'),
)

TEMPLATE_DIRS = (
    os.path.join(BASE_DIR, 'templates'),

)

TEMPLATE_CONTEXT_PROCESSORS = (
    'django.core.context_processors.request',
    'django.contrib.auth.context_processors.auth',
    'django.core.context_processors.debug',
    'django.core.context_processors.media',
    'django.core.context_processors.static',
    'django.contrib.messages.context_processors.messages',
    'django.core.context_processors.i18n',
)

from . private_settings import DB_NAME, DB_PASSWORD, DB_USER, DB_PORT, DB_HOST

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': DB_NAME,
        'USER': DB_USER,
        'PASSWORD': DB_PASSWORD,
        'HOST': DB_HOST,
        'PORT': DB_PORT,
    }
}

# DATABASES = {
#     'default': {
#         'ENGINE': 'django.db.backends.sqlite3',
#         'NAME': os.path.join(BASE_DIR, 'devel.db'),
#     }
# }

# Internationalization
# https://docs.djangoproject.com/en/1.6/topics/i18n/

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True

#####################################
##### Django DB Backup Settings #####
#####################################
DBBACKUP_BACKUP_DIRECTORY = os.path.join(BASE_DIR, "db_backup")

###########################
##### Celery Settings #####
###########################
#CELERY_RESULT_BACKEND = "amqp"
BROKER_URL = 'django://'

import djcelery
djcelery.setup_loader()

from celery.schedules import crontab

CELERYBEAT_SCHEDULE = {
    # crontab(hour=0, minute=0, day_of_week='saturday')
    'backup-database': {  # example: 'file-backup'
        'task': 'apps.products.tasks.backup_database',  # example: 'files.tasks.cleanup'
        'schedule': crontab(minute=0, hour='13', day_of_week='wed,sat')
    },
}

CELERYBEAT_SCHEDULER = 'djcelery.schedulers.DatabaseScheduler'

##########################
##### Email Settings #####
##########################
EMAIL_HOST = 'smtp.gmail.com'
EMAIL_HOST_USER = 'Frz6Jbf9@gmail.com'
EMAIL_HOST_PASSWORD = 'mbveibezwkwkjmon'
EMAIL_PORT = 587
EMAIL_USE_TLS = True
DEFAULT_FROM_EMAIL = EMAIL_HOST_USER
ADMIN_EMAIL = "admin@goodobtain.com"

SALE_EMAIL_USER = 'lapakhpcase@gmail.com'
SALE_EMAIL_PASSWORD = '**NYj6rPSgQG1/FTKnJh4xfPVXSH?6Dr'

  # Absolute path to the directory static files should be collected to.
  # Don't put anything in this directory yourself; store your static files
  # in apps' "static/" subdirectories and in STATICFILES_DIRS.
  # Example: "/var/www/example.com/static/"
STATIC_ROOT = 'static'

# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.6/howto/static-files/
STATIC_URL = '/static/'
LOGIN_URL = "/users/signin/"

STATICFILES_DIRS = (
    # Put strings here, like "/home/html/static" or "C:/www/django/static".
    # Always use forward slashes, even on Windows.
    # Don't forget to use absolute paths, not relative paths.
    os.path.join(BASE_DIR, 'staticfiles'),
)

# List of finder classes that know how to find static files in
# various locations.
STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
)

# Allow any settings to be defined in local_settings.py which should be
# ignored in your version control system allowing for settings to be
# defined per machine.
try:
    from local_settings import *
except ImportError:
    pass

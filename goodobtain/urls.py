from django.conf import settings
from django.conf.urls import patterns, include, url
from django.contrib import admin

from products.views import AdminCrawlSpiderView, AdminScrapyFormView
from . views import IndexView

admin.autodiscover()

urlpatterns = patterns('',
    url(r'^$', IndexView.as_view()),

    url(r'^admin/scrapy-crawl/(?P<spider>.*\w+)/$', AdminCrawlSpiderView.as_view(), name='crawl-spider'),
    url(r'^admin/scrapy/$', AdminScrapyFormView.as_view(), name='scrapy'),
    url(r'^admin/', include(admin.site.urls)),

    url(r'^products/', include('products.urls', namespace='products')),
    url(r'^crawler/', include('crawler.urls')),
    url(r'^users/', include('users.urls', namespace='users')),
    url(r'^captcha/', include('captcha.urls')),
)

if settings.DEBUG:
    try:
        import debug_toolbar
        urlpatterns += patterns('',
            url(r'^__debug__/', include(debug_toolbar.urls)),
        )
    except ImportError:
        pass

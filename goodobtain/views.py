from django.views.generic.base import RedirectView


class IndexView(RedirectView):

    permanent = False
    url = "/products/"

# INSTALL tutorial

Following steps were performed while moving to new droplet.

- ### [Installation on DigitalOcean‎ with serverpilot](do_serverpilot_install.md)

## As root user:

  apt-get update
  sh deb-install.sh

  apt-get install supervisor
  ln -s /home/goodobtain/site-aggregation/conf/goodobtain.supervisor.conf \
     /etc/supervisor/conf.d/goodobtain.supervisor.conf 
  supervisorctl reload 

  apt-get install nginx
  ln -s /home/goodobtain/site-aggregation/conf/goodobtain.nginx.conf \
      /etc/nginx/sites-enabled/
  service nginx restart

  adduser goodobtain

Add database (Postgresql)

  sudo -u postgres psql
  create database database_name;
  CREATE USER database_user WITH password 'database_password';
  GRANT ALL privileges ON DATABASE database_name TO database_user;

  <!--sudo apt-get install postgresql postgresql-contrib-->
  <!--sudo -i -u postgres-->
  <!--createdb goodobtain-->
  <!--psql -->
    <!--CREATE USER xling WITH PASSWORD 'test_password';-->
    <!--GRANT ALL PRIVILEGES ON DATABASE "goodobtain" to xling;-->
    <!--\q-->

Add user *goodobtain* to *sudo* group

    gpasswd -a goodobtain sudo

Allow to user *goodobtain* run **supervisorctl** without password

    echo "goodobtain ALL=(ALL) NOPASSWD: /usr/bin/supervisorctl" > /etc/sudoers.d/goodobtain 


## As goodobtain user:

    ssh-keygen

* Add /home/goodobtain/.ssh/id_rsa.pub to
https://bitbucket.org/leonexu/site-aggregation/admin/deploy-keys/ 


  git clone git@bitbucket.org:leonexu/site-aggregation.git
  cd site-aggregation
  virtualenv venv
  source venv/bin/activate
  pip install -r requirements.txt
  mkdir log
  nano /home/goodobtain/site-aggregation/goodobtain/private_settings.py
      DB_NAME = 'database_name'
      DB_PASSWORD = 'database_password'
      DB_USER = 'database_user'
      DB_PORT = ''
  pip install django-debug-toolbar
  python manage.py migrate
  python manage.py runserver
  fab migrate


* On the devel machine:
  pip install -r requirements-devel.txt
  python manage.py createsuperuser
  python manage.py loaddata apps/products/fixtures/website.json
  python manage.py loaddata apps/products/fixtures/category.json
  python manage.py loaddata apps/products/fixtures/product.json
  python manage.py loaddata apps/products/fixtures/metrics.json

  <!--fab manage:createsuperuser -->
  <!--fab manage:"loaddata apps/products/fixtures/website.json"-->
  <!--fab manage:"loaddata apps/products/fixtures/category.json"-->
  <!--fab manage:"loaddata apps/products/fixtures/product.json"-->
  <!--fab manage:"loaddata apps/products/fixtures/metrics.json"-->

# Q&A 

## Receive "502 Bad Gateway" when visit the site

* Cause: issue was supervisor was not properly installed and configured and was not running as daemon as required to auto restart nginx django etc

* Solution:
Change /etc/init/supervisord.conf to the follows
  description "supervisord"

  start on runlevel [2345]
  stop on runlevel [!2345]

  respawn

  exec /usr/local/bin/supervisord --nodaemon --configuration /etc/supervisord.conf 

sudo systemctl enable supervisor
sudo service supervisor restart

## How to set the speed of each spider
In scrapy_shopcrawler / mycrawler / spiders / tokopedia_spider.py

after
allowed_domains = ['tokopedia.com']

custom_settings = {
'CONCURRENT_REQUESTS': 90,
'CONCURRENT_REQUESTS_PER_DOMAIN': 90,
}

#TODO
review and fix install.sh - https://trello.com/c/YYR1cwi2/9-fix-install-sh-and-initial-setup 

If you are unable to use (pip install cryptography) then the alternative:
    
    sudo apt-get install python-cryptography

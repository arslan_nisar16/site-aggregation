te +%A`
#THEDATE="Today"
month_day=`date +"%d"`
week_day=`date +"%u"`

# On first month day do
#if [ "$month_day" -eq 1 ] ; then
  THEDATE="Monthly"
#else
  # On saturdays do
if [ "$week_day" -eq 6 ] ; then
    THEDATE="Weekly"
else

    # On any regular day do
THEDATE=day_$(($week_day%3))
fi
#fi

su - 'postgres' -c "pg_dumpall" > all.sql

tar -czvf /home/goodobtain/site-aggregation/database-backup/all_"$THEDATE".tar.gz /root/all.sql
tar -czvf /home/goodobtain/job/codes/goodobtain/database-backup/all_"$THEDATE".tar.gz /root/all.sql
rm /root/all.sql

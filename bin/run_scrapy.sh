#!/bin/sh
FILE_PATH=`readlink -e $0`
BIN_DIR_PATH=`dirname $FILE_PATH`
PROJECT_ROOT=`dirname $BIN_DIR_PATH`
SCRAPY_ROOT="$PROJECT_ROOT/scrapy_shopcrawler"

SPIDERS="bukalapak_all_products lazada_all_products tokopedia_all_products"

#SPIDERS="tokopedia_update_products "
# bukalapak_update_products \
  # lazada_category"

cd $SCRAPY_ROOT
for spider in $SPIDERS; 
do
  echo $spider
  cmd="$PROJECT_ROOT/venv/bin/scrapy \
    crawl $spider \
    -s DOWNLOAD_DELAY=1 \
    -s JOBDIR=jobs/$spider \
    -s LOG_LEVEL=INFO"
  #echo $cmd;
  pgrep --full "$cmd"
  if [ "$?" -eq "0" ]
  then
    echo "$spider is already running. \nExit."
    exit 0
  else
    $cmd;
  fi
  sleep 1;
done


#!/bin/sh
FILE_PATH=`readlink -e $0`
BIN_DIR_PATH=`dirname $FILE_PATH`
PROJECT_ROOT=`dirname $BIN_DIR_PATH`

export DJANGO_SETTINGS_MODULE="goodobtain.settings"

$PROJECT_ROOT/venv/bin/python $PROJECT_ROOT/manage.py update_selling_speed 


#!/usr/bin/env python
# -*- coding: utf-8 -*-
import os
from contextlib import contextmanager as _contextmanager
from contextlib import nested
from fabric.api import (
    cd,
    env,
    local,
    run,
    settings,
    sudo,
    task,
    prefix,
    shell_env,
    abort,
)
from fabric.contrib.console import confirm
from fabric.colors import red, green

supervisor_app_name = project_name = 'goodobtain'
deploy_user = 'goodobtain'
production_branch = 'master'

env.use_ssh_config = True
env.hosts = ['goodobtain.com']
env.user = deploy_user
env.project_root = '/home/goodobtain/site-aggregation'
env.scrapy_root = '%(project_root)s/scrapy_shopcrawler'
env.virtualenv = '%(project_root)s/venv' % env
env.django_settings_module = 'goodobtain.settings'


@_contextmanager
def virtualenv():
    with prefix('. %(virtualenv)s/bin/activate' % env):
        yield


def get_current_branch():
    return str(local('git rev-parse --abbrev-ref HEAD', capture=True))


def is_production():
    """
        Detect if command is run on the production server.
        devel_settings.py is present only on develepment machine
        so if devel_settings.py is absent then we can asume it's
        production server
    """
    return not os.path.exists('goodobtain/devel_settings.py')


def production_env():
    """ Production environment """
    env.django_settings_module = 'goodobtain.settings'


def devel_env():
    """ Development environment """
    env.django_settings_module = 'goodobtain.devel_settings'
    env.project_root = os.path.dirname(os.path.abspath(__file__))
    env.scrapy_root = '%(project_root)s/scrapy_shopcrawler' % env
    env.virtualenv = '%(project_root)s/venv' % env


def test_env():
    """ Test environment """
    env.django_settings_module = 'goodobtain.test_settings'
    env.project_root = os.path.dirname(os.path.abspath(__file__))
    env.virtualenv = '%(project_root)s/venv' % env


@task
def deploy():
    """ Deploy changes to production server """
    current_branch = get_current_branch()
    if current_branch != production_branch:
        print(red('Your current branch is "%s"' % current_branch))
        print(green('Switch to branch "%s" for deploying changes to production server' % production_branch))
        abort('Not %s branch' % production_branch)
    answer = confirm("This will update server. Continue?", False)
    if not answer:
        return
    local('git pull origin %s' % production_branch)
    local('git push')
    with cd(env.project_root), settings(sudo_user=deploy_user), virtualenv():
        run('git pull origin %s' % production_branch)
        run('pip install -r requirements.txt')
        run('python manage.py migrate')
        run('python manage.py collectstatic --noinput')
        run('git status')
    sudo('supervisorctl restart %s' % supervisor_app_name, shell=False)
    #if confirm("Do you want to restart Celery?", False):
    #    sudo('supervisorctl restart celery', shell=False)
    sudo('supervisorctl status', shell=False)


@task
def manage(command):
    """Run django management commands"""
    devel_env()
    with shell_env(DJANGO_SETTINGS_MODULE='%(django_settings_module)s' % env), virtualenv():
        local("python manage.py %s" % command)


@task
def shell():
    """django shell"""
    manage('shell')


@task
def migrate(app='', param=''):
    """Update the database schema"""
    manage("migrate  --noinput %s %s" % (app, param))


@task
def collectstatic():
    """Collects static files from each of your applications"""
    manage('collectstatic --noinput')


@task
def makemigrations(app=''):
    """Create new migrations and migrate"""
    print "Migrate app: %s" % app
    print "Usage:\n\t makemigrations:app_name to create migration for specified application."
    manage("makemigrations %s" % app)
    migrate(app)


@task
def test(app='core customers products users', param='-v 1'):
    """Run django apps' tests."""
    # TODO add ecmanager to 'app' param 
    test_env()
    reuse_db = os.environ.get('REUSE_DB', 1)
    with shell_env(DJANGO_SETTINGS_MODULE='%(django_settings_module)s' % env), virtualenv():
        local(
            'REUSE_DB=%s '
            'python manage.py test '
            '--failfast '
            '--keepdb '
            #"--with-coverage "
            #"--cover-html "
            '%s %s' % (reuse_db, param, app)
        )


@task
def coverage(app='core customers products users', param='--failfast'):
    """
    Runs the test suite for the specified applications - currently unavailable
    Then generates coverage report both onscreen and as HTML
    """
    abort('"coverage" command is currently unavailable')
    #manage("test_coverage %s %s" % (app, param))


@task(alias='run')
def runserver(param='0.0.0.0:8000'):
    """Run dev server"""
    manage("runserver %s" % param)


@task(name='start')
def start_spider(spider):
    """Run crawler"""
    abort('"start" command is currently unavailable')
    return
    if is_production():
        production_env()
    else:
        devel_env()

    with nested(cd(env.scrapy_root), virtualenv()):
        local('pwd')
        local('scrapy crawl %s' % spider)

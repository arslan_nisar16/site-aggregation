# -*- coding: utf-8 -*-

from mycrawler.models import Product

for product in Product.select():
  title = product.title
  if title[0] == '\n': 
    title = title[1:]
  if title[-1] == '\n': 
    title = title[:-1]
  product.title = title
  product.save()

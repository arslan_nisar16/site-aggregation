#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Dump product metrics from database
'''
Usage: 
  Output the data in shop (assigned by shop_url) into output_file_name
Call: 
  python -m result_visualizer.shop_dump shop_url --csv output_file_name --data likes
'''

import os
import argparse
import csv
import cStringIO
import codecs

from mycrawler.models import mysql_db, Product, Metrics, Shop

ROOT_DIR = os.path.abspath(os.path.dirname(__file__))
TIME_FORMAT  = '%m/%d/%Y'

class UnicodeWriter:

  def __init__(self, f, dialect=csv.excel, encoding="utf-8", **kwds):
    self.queue = cStringIO.StringIO()
    self.writer = csv.writer(self.queue, **kwds)
    self.stream = f
    self.encoder = codecs.getincrementalencoder(encoding)()

  def writerow(self, row):
    self.writer.writerow(map(lambda s: s.encode("utf-8"), row))
    data = self.queue.getvalue()
    data = data.decode("utf-8")
    data = self.encoder.encode(data)
    self.stream.write(data)
    self.queue.truncate(0)

def write_csv(filename, data):
  with open(filename, 'wb') as f:
    writer = UnicodeWriter(f, quoting=csv.QUOTE_ALL)
    for line in data:
      writer.writerow(line)

def get_products(shop_url, field_to_extract):
  data = {}
  product_urls = []
  mysql_db.connect()
  try:
    shop = Shop.objects.filter(url=shop_url.split('?')[0])
    print 'Products found: %s' % shop.products.count()
    for p in shop.products:
      product_urls.append(p.url)
      for m in p.metrics.order_by(Metrics.timestamp):
        timestamp = m.timestamp.replace(
          hour=0, minute=0, second=0, microsecond=0
        )
        data.setdefault(timestamp, {})
        data[timestamp][p.url] = str(getattr(m, field_to_extract, ''))
  except Shop.DoesNotExist:
    print 'Shop with a given URL cannot be found :: %s' % shop_url
  else:
    sheet = [[''] + product_urls]
    for i, timestamp in enumerate(sorted(data.keys())):
      sheet.append([timestamp.strftime(TIME_FORMAT)])
      for url in product_urls:
        m = data[timestamp].objects.filter(url, '')
        sheet[i+1].append(m)
    return sheet
  finally:
    mysql_db.close()

def ExportKeywordTable():
  '''
  [](http://stackoverflow.com/questions/5941809/include-headers-when-using-select-into-outfile)

[export into file]

SELECT 'keyword', 'averageCpc', 'productNumber', 'searchVolume', 'competition' UNION ALL SELECT keyword, averageCpc, productNumber, searchVolume, competition FROM keyword INTO OUTFILE '/tmp/keyword.csv' fields terminated by ',';

[export into file, sort, delimiter]
select 'keyword', 'averageCpc', 'productNumber', 'searchVolume', 'competition', 'score' UNION ALL SELECT keyword, averageCpc, productNumber, searchVolume, competition, searchVolume/productNumber as score FROM keyword order by score desc into outfile '/tmp/keyword5.csv' fields terminated by ',';

[export into file, sort, delimiter, limit row number]
SELECT 'keyword', 'averageCpc', 'productNumber', 'searchVolume', 'competition', 'score' UNION ALL SELECT keyword, averageCpc, productNumber, searchVolume, competition, searchVolume/productNumber as score FROM keywords where searchVolume > 0 order by score desc limit 1000 into outfile '/tmp/keyword10.csv' fields terminated by ',';

[export into file, sort, delimiter, limit row number, exclude non-buyer keywords]
SELECT 'keyword', 'averageCpc', 'productNumber', 'searchVolume', 'competition', 'score' UNION ALL SELECT keyword, averageCpc, productNumber, searchVolume, competition, searchVolume/productNumber as score FROM keywords where produc tNumber>36 and keyword not like '%trend%' and keyword not like '%harga%' and keyword not like '%download%' and keyword not like '%free%' and keyword not like '%online%' and keyword not like '%cara%' order by score desc limit 1000 into outfile '/tmp/keywo
rd16.csv' fields terminated by ',';
  '''
  pass

if __name__ == '__main__':
  parser = argparse.ArgumentParser(description='Shop::Dump')
  parser.add_argument('shop_url')
  parser.add_argument(
    '--data',
    help='Specify the field to extract (default: "likes")',
    default='likes',
  )
  parser.add_argument(
    '--csv',
    help='Write output into .csv file with a given name (default: "product.csv")',
    default='product.csv'
  )
  parser.add_argument(
    '--dir',
    help='Save .csv file in this directory (default: "output")',
    default='output',
  )
  args = parser.parse_args()
  shop_url = args.shop_url
  field_to_extract = args.data.lower()
  if field_to_extract.startswith('_') or field_to_extract not in Metrics.__dict__:
    print 'No such field :: %s' % field_to_extract
    raise SystemExit
  print 'Searching for products :: by "%s" field :: in %s shop' % (field_to_extract, shop_url)
  products = get_products(shop_url, field_to_extract)
  if products:
    if args.csv:
      csv_file = args.csv if args.csv.endswith('.csv') else args.csv + '.csv'
      dir = os.path.join(ROOT_DIR, args.dir)
      if not os.path.exists(dir):
        os.mkdir(dir)
      filename = os.path.join(dir, csv_file)
      print '*' * 40
      print 'All products saved to %s' % filename
      print '*' * 40
      write_csv(filename, products)
    else:
      print (
        "Since all URLs have different lenght, "
        "it's pretty hard to print a user-friendly table. "
        "Use .csv file as output instead."
        )

# -*- coding: utf-8 -*-

import django
from mycrawler.extended.bukalapak.ec_market_analyzer import EcMarketAnalyzer
from mycrawler.extended.price_comparison.price_comparison import PriceComparison
from mycrawler.itemloader import DefaultItemLoader
from mycrawler.items import ShopItem
from mycrawler.spiders import queryPaths
from products.models import Category
from products.models import Website, Product
from scrapy import Spider, Request
from scrapy.linkextractors import LinkExtractor
from scrapy.spiders import Rule

django.setup()


class BukaSpider(Spider):
    name = 'bukalapak_old'
    allowed_domains = ['bukalapak.com']
    # Personal per spider limit overriding global settings.py
    custom_settings = {
        'CONCURRENT_REQUESTS': 90,
        'CONCURRENT_REQUESTS_PER_DOMAIN': 90,
    }

    def __init__(self, service="", *args, **kwargs):
        super(BukaSpider, self).__init__(*args, **kwargs)
        self.service = service

    def start_requests(self):
        self.logger.debug("self.service: %s" % self.service)

        if self.service == 'CollectShopsFromKeywords':
            from mycrawler.private_settings import seedKeywords
            from mycrawler.extended.bukalapak.shop_collector import ShopCollector
            self.logger.debug("CollectShopsFromKeywords")
            shopCollector = ShopCollector(
                queryPaths['buka']['search']['inputbox'],
                queryPaths['buka']['search']['shopLinksOnCurrentResultPage'],
                nextShopPagePath=queryPaths['buka']['search']['nextShopPage'],
            )
            for x in shopCollector.CollectShopsFromKeywords(self.logger, seedKeywords):
                yield x

        elif self.service == 'CrawlMyShop':
            for request in EcMarketAnalyzer.RecordProductsInShops(self.logger, self.GetSeedShopUrlsFromSettings()):
                yield request

        elif self.service == 'CrawlShopsInDb':
            for request in EcMarketAnalyzer.RecordProductsInShops(self.logger, self.GetShopUrlsFromDb()):
                yield request

        elif self.service == 'UpdateProductMetrics':
            for request in self.UpdateProductMetrics():
                yield request

        elif self.service == 'UpdateStatusOfShopsInDb':
            self.logger.debug('UpdateStatusOfShopsInDb')
            # from apps.products.models import Shop
            from products.models import Shop
            EcMarketAnalyzer.logger = self.logger
            for shop in Shop.select().distinct():
                try:
                    self.logger.debug("shop.url: %s" % shop.url)
                    request = Request(shop.url, callback=self.UpdateStatusOfShopsInDb)
                except Exception as e:
                    import sys
                    self.logger.error('Error on line {}'.format(sys.exc_info()[-1].tb_lineno))
                    self.logger.error('str(e) %s' % str(e))
                    self.logger.error(e.message)
                    raise e
                yield request

        elif self.service == 'AnalyzeKeyword':
            from mycrawler.keyword_crawler.keyword_crawler import KeywordCrawler
            keywordCrawler = KeywordCrawler()
            for request in keywordCrawler.CrawlKeywords():
                yield request

            # It takes time for parse_xxx to issue Items into pipeline (the html code takes time to return).
            # The crawler stop immediately if pipeline does not have Items.
            # Need to wait for a while manually to prevent the crawler stopping.
            import time
            time.sleep(5)

        elif self.service == 'PriceComparison':
            # Dont know why but native python logger cannot be used in price_comparison.py.
            # I need to borrow scrapy's logger.
            for request in PriceComparison.ComparePrice(self.logger):
                yield request

    def GetSeedShopUrlsFromSettings(self):
        shopUrls = []
        from mycrawler.settings import ShopUrls as shopUrls
        assert len(shopUrls)
        return shopUrls

    def GetShopUrlsFromDb(self):
        # from apps.products.models import Shop
        from products.models import Shop
        shopUrls = [shop.url for shop in Shop.select(Shop.url)]
        assert len(shopUrls)
        return shopUrls

    def UpdateProductMetrics(self):
        # from apps.products.models import Product, Metrics
        from products.models import Product, Metrics
        EcMarketAnalyzer.logger = self.logger
        for product in Product.select().join(Metrics).where(Metrics.price > 140000, Metrics.sales > 30).distinct():
            self.logger.debug("product.url: %s" % product.url)
            request = Request(
                product.url,
                callback=EcMarketAnalyzer.parse_Product,
                meta={
                    'data': {
                        'shopReference': None
                    },
                },
            )
            yield request

    def UpdateStatusOfShopsInDb(self, response):
        try:
            self.logger.debug("UpdateStatusOfShopsInDb")
            shopItem = DefaultItemLoader(ShopItem(), response=response)

            shopItem.add_value('url', response.url)

            from mycrawler.extended.bukalapak import ParseFeedback
            feedbackNumber, score = ParseFeedback(response, cls.logger)
            shopItem.add_value('feedbackNumber', feedbackNumber)
            shopItem.add_value('score', score)

            shopItem = shopItem.load_item()
        except Exception as e:
            import sys
            self.logger.error('Error on line {}'.format(sys.exc_info()[-1].tb_lineno))
            self.logger.error('str(e) %s' % str(e))
            self.logger.error(e.message)
        else:
            yield shopItem


class BukaAllShopSpider(Spider):
    name = 'bukalapak_old_allShopSpider'
    allowed_domains = ['bukalapak.com']
    # Personal per spider limit overriding global settings.py
    custom_settings = {
        'CONCURRENT_REQUESTS': 90,
        'CONCURRENT_REQUESTS_PER_DOMAIN': 90,
    }

    start_urls = ['https://www.bukalapak.com/c/buku']
    rules = (
        Rule(
            LinkExtractor(allow="^https:\/\/www\.bukalapak\.com\/p\/.*$"),
            callback="parse_product",
            follow=True
        ),
    )

    def parse(self, response):
        for url in response.xpath(
                "//li[contains(@class, 'js-tree-item') and not(contains(@class, 'has-children'))]/a/@href"
        ).extract():
            url = response.urljoin(url)
            yield EcMarketAnalyzer.ToCategoryProducts(self.logger, url)


class BukalapakCategorySpider(Spider):
    """ Crawl all active bukalapak.com categories """
    name = 'bukalapak_all_products'  # 'bukalapak_category'
    website_domain = 'www.bukalapak.com'
    allowed_domains = ['bukalapak.com']
    # Personal per spider limit overriding global settings.py
    custom_settings = {
        'CONCURRENT_REQUESTS': 90,
        'CONCURRENT_REQUESTS_PER_DOMAIN': 90,
    }

    def start_requests(self):
        self.website = Website.objects.filter(base_url=self.website_domain).first()
        if not self.website:
            self.logger.error("Website with base_url {} does not exist".format(
                self.website_domain
            ))
            return
        for cat in Category.objects.filter(active=True, website=self.website):
            yield Request(
                cat.url,
                callback=self.parse,
                meta={"category_id": cat.id}
            )

    def parse(self, response):
        for url in response.xpath(
                "//li[contains(@class, 'js-tree-item--active')]/."
                "//li[contains(@class, 'js-tree-item')"
                "and not(contains(@class, 'has-children'))]/a/@href"
        ).extract():
            url = response.urljoin(url)
            yield EcMarketAnalyzer.ToCategoryProducts(
                self.logger,
                url,
                cat_meta=response.meta
            )


class BukalapakUpdateProductsSpider(Spider):
    """ Crawl ONLY products that are alredy present in the database """
    name = 'bukalapak_existing_products_only'  # 'bukalapak_update_products'
    website_domain = 'www.bukalapak.com'
    allowed_domains = ['bukalapak.com']
    # Personal per spider limit overriding global settings.py
    custom_settings = {
        'CONCURRENT_REQUESTS': 90,
        'CONCURRENT_REQUESTS_PER_DOMAIN': 90,
    }


    def start_requests(self):
        self.website = Website.objects.filter(base_url=self.website_domain).first()
        if not self.website:
            self.logger.error("Website with base_url {} does not exist".format(
                self.website_domain
            ))
            return

        product_queryset = Product.objects.filter(
            category__active=True,
            category__website=self.website,
        ).to_crawl()
        for product in product_queryset:
            url = product.url
            yield Request(
                url,
                callback=self.parse,
                meta={"data": {"category_id": product.category.id}}
            )

    def parse(self, response):
        self.logger.debug("parse_product")
        return EcMarketAnalyzer.parse_product(response, logger=self.logger)

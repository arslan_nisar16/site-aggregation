# -*- coding: utf-8 -*-

import json
import urlparse

from products.models import Product, Category, Website
from scrapy import Request
from scrapy import Spider

from .. extended.lazada.util import PriceStringToNumber
from .. items import MetricsItem
from .. settings import DEBUG_MODE


class MissingWebsiteException(Exception):
    pass


class BaseTokopediaSpider(Spider):
    website_domain = 'www.tokopedia.com'
    allowed_domains = ['tokopedia.com']

    category_product_num = 50
    # sort by sales: ob=8
    product_list_url = 'https://ace.tokopedia.com/search/v1/product' \
                       '?sc={cat_id}&rows=50&start={start_num}&terms=true&ob=8'

    def __init__(self):
        self._get_website()

    def _get_website(self):
        self.website = Website.objects.filter(base_url=self.website_domain).first()
        if self.website is None:
            self.logger.error("Website with base_url {} does not exist".format(
                self.website_domain
                ))
            raise MissingWebsiteException()

    def _get_json_response(self, response):
        data = json.loads(response.body_as_unicode())
        return data['data']

    def parse(self, response):
        try:
            cat_id = response.xpath('//script/text()').re('"categoryId":"(\d+)"')[0]
        except IndexError:
            self.logger.error("Could not find categoryId on page {}".format(
                response.url))
            return
        meta = {
            "start_num": 0,
            "cat_id": cat_id,
            "db_cat_id": response.meta['db_cat_id']
        }
        url = self.product_list_url.format(**meta)
        return Request(url, callback=self.parse_category, meta=meta)

    def parse_category(self, response):
        data = self._get_json_response(response)
        if not data:
            return
        for product in data:
            meta = {"product": product, "db_cat_id": response.meta['db_cat_id']}
            yield Request(product['uri'], callback=self.parse_product, meta=meta)

        meta = response.meta
        meta['start_num'] += self.category_product_num
        url = self.product_list_url.format(**meta)
        yield Request(url, callback=self.parse_category, meta=meta)

    def _parse_info_int(self, val):
        try:
            multipl = 1
            if 'rb' in val:
                multipl = 1000
            val = val.replace(',', '.').replace('rb', '')
            val = int(float(val)*multipl)
            return val
        except StandardError:
            return 0

    def parse_product(self, response):
        parts = list(urlparse.urlparse(response.url))
        parts[4] = ''
        product_url = urlparse.urlunparse(parts)
        product = Product.objects.filter(url=product_url).first()
        if product is None:
            product = Product(url=product_url)
        product_data = response.meta.get('product')
        if product_data:
            product.internalId = product_data['id']
            product.title = product_data['name']
            product.imagePath = product_data['image_uri']
            price = product_data['price']
        else:
            product.title = response.xpath(
                '//h1[contains(@class, "product-title")]/a/text()'
            ).extract_first()

            product.internalId = response.xpath(
                '//input[@id="product-id"]/@value'
            ).extract_first()

            product.imagePath = response.xpath(
                '//div[@class="product-imagebig"]/a/@href'
            ).extract_first()

            price = response.xpath(
                '//span[@itemprop="price"]/text()'
            ).extract_first()

        price = PriceStringToNumber(self.logger, price[3:])

        product_category_id = response.meta['db_cat_id']
        if product.category_id != product_category_id:
            product.category_id = product_category_id
        try:
            product_block = response.xpath(
                '//div[contains(@class, "product-content-container")]'
            )[0]
        except IndexError:
            self.logger.info("Product has no product block {}", product_url)
            return
        try:
            info_block = product_block.xpath('.//dl')[0]
        except IndexError:
            self.logger.info("Product has no info block {}", product_url)
            return

        product.description = " ".join(product_block.xpath(
            './/p[@itemprop="description"]/text()'  # './p[@itemprop="description"]/descendant-or-self::*/text()'
        ).extract())

        sales = info_block.xpath(
            '//div/dl/dd[3]/text()'  # 'dt[contains(text(), "Terjual")]/following-sibling::dd[1]/text()'
        ).extract_first()
        if not DEBUG_MODE:
            product.save()
        try:
            metrics_item = MetricsItem(
                price=price,
                sales=self._parse_info_int(sales),
                product=product
                )
        except KeyError as err:
            self.logger.warning(
                "Can not fetch info from metrics {} for url {}".format(str(err), response.url)
            )

            return
        view_url = 'https://www.tokopedia.com/provi/check?pid={pid}'.format(pid=product.internalId)
        yield Request(view_url, callback=self.parse_metric_view, meta={"metric_item": metrics_item})

    def parse_metric_view(self, response):
        metric = response.meta['metric_item']
        try:
            metric['pageviews'] = int(response.xpath('*').re(r'"view":\s*(\d+)')[0])
        except StandardError:
            self.logger.exception("Can not fetch view for product {}".format(response.url))
            return
        product = metric.get('product')
        sales_url = 'https://js.tokopedia.com/productstats/check?pid={pid}'.format(pid=product.internalId)
        yield Request(sales_url, callback=self.parse_metric_sales, meta={"metric_item": metric})

    def parse_metric_sales(self, response):
        metric = response.meta['metric_item']
        try:
            metric['sales'] = int(response.xpath('*').re(r'"item_sold":\s*(\d+)')[0])
        except StandardError:
            self.logger.exception("Can not fetch sales for product {}".format(response.url))
            return
        return metric


class TokopediaCategorySpider(BaseTokopediaSpider):
    name = 'tokopedia_all_products'  # 'tokopedia_category'

    def start_requests(self):
        for cat in Category.objects.filter(active=True, website=self.website):
            meta = {"db_cat_id": cat.id}
            yield Request(cat.url, callback=self.parse, meta=meta)


class TokopediaUpdateProductsSpider(BaseTokopediaSpider):
    name = 'tokopedia_existing_products_only'  # 'tokopedia_update_products'

    def start_requests(self):
        product_queryset = Product.objects.filter(
            category__active=True,
            category__website=self.website,
        ).to_crawl()

        for product in product_queryset:
            url = product.url
            meta = {"db_cat_id": product.category.id}
            yield Request(url, callback=self.parse_product, meta=meta)

# This package will contain the spiders of your Scrapy project
#
# Please refer to the documentation for information on how to create and manage
# your spiders.

queryPaths = {
    'buka': {
      'search': {
          'inputbox': 'https://www.bukalapak.com/products?utf8=%E2%9C%93&search%5Bkeywords%5D=%s',
          'shopLinksOnCurrentResultPage': "//h5[@class='user__name']/a/@href",
          'nextShopPage': ['//a[@class="next_page"]/@href'], # the link to the 'next page' button
        } 
      },
    'toko': {
      'search': {
          'inputbox': 'https://www.tokopedia.com/search?st=product&q=%s&sc=0',
          'shopLinksOnCurrentResultPage': "//div[@class='span8 no-minheight ellipsis pull-left']/small/a/@href",
          'ithPage': ['//div[@class="pagination pull-right"]/ul/li[%s]/a/@href'], #the 'next page' botton does not exist. The link to each page instead.
        } 
      }
    }



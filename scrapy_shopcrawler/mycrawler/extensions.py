import subprocess

import logging
from scrapy import signals
from scrapy.exceptions import NotConfigured

logger = logging.getLogger(__name__)


class RunCommandExtension(object):
    def __init__(self, spider_commands):
        self.spider_commands = spider_commands

    @classmethod
    def from_crawler(cls, crawler):
        if not crawler.settings.getbool('RUNCOMMANDS_EXT_ENABLED'):
            raise NotConfigured

        ext = cls(crawler.settings.get('RUNCOMMANDS_EXT_COMMANDS'))

        crawler.signals.connect(ext.spider_closed, signal=signals.spider_closed)

        return ext

    def spider_closed(self, spider):
        cmd = self.spider_commands.get(spider.name)
        if cmd:
            logger.info("Running command from spider {}: {}".format(
                spider.name, cmd))
            subprocess.Popen(cmd, shell=True)


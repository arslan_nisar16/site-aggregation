import os

from django.conf import settings

from apps.products.models import Product, Metrics
# from scrapy_shopcrawler.mycrawler.extended.keyword_crawler.related_keyword_finder import RelatedKeywordFinder


class Ecommerce(object):

    def __init__(self, spider=None):
        self.spider = spider

    def crawl_products_from_keywords(self, keywords):
        """
        for each product in db that is related to the given keywords:
            RecordMetrics(product)
        """
        # products = Product.objects.all()
        # for product in products:
        pass

    def crawl_all_products(self):
        """
        for each product in db:
            RecordMetrics(product)
        """
        products = Product.objects.all()
        for product in products:
            Metrics.objects.create(
                product=product,
                price=product.retailPrice
            )

    def PeriodicallyCrawlProducts(self):
        pass

    def get_keywords(self, seed_keywords):
        """
        From a set of seed keywords, generate lots of related keywords and save into db.
        """
        # finder = RelatedKeywordFinder()
        # finder.GatherPlaceholderKeywordsFromSeedsAndWriteIntoDb(seed_keywords)
        pass


    def FindProductsRelatedToKeywords(self):
        """
        Load keywords from db
        Find all products related to this keywords on the ec site and save into db.
        """
        pass

    def find_all_products(self):
        """
        Crawl all products in this site randomly and save into the db.
        """
        command = "%s %s" % ("scrapy crawl", self.spider)
        scrapy_project = settings.SCRAPY_DIR
        os.chdir(scrapy_project)
        os.system(command)


class Buka(Ecommerce):

    def __init__(self):
        self.spider = "allShopSpider"


class Lazada(Ecommerce):

    def __init__(self):
        self.spider = "lazadaAllShopSpider"


class Toko(Ecommerce):

    def __init__(self):
        self.spider = "tokoSpider"

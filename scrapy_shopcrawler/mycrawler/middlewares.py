# -*-coding:utf-8-*-

import time
from random import choice
from scrapy.loader import arg_to_iter
from scrapy.downloadermiddlewares.useragent import UserAgentMiddleware
from .navigator import generate_navigator


class RotateUserAgentMiddleware(UserAgentMiddleware):

    def __init__(self, user_agent):
        self.user_agent = user_agent

    def process_request(self, request, spider):
        user_agent = generate_navigator()['user-agent']
        request.headers.setdefault('user-agent', user_agent)
        spider.logger.debug(
            '%s User-Agent: %s' % (request, request.headers['user-agent'])
        )


class ProxyMiddleware(object):

    def __init__(self, proxies):
        if proxies:
            self.proxies = arg_to_iter(proxies)
            self._active = True
        else:
            self._active = False

    def process_request(self, request, spider):
        if self._active:
            request.meta['proxy'] = choice(self.proxies)
            spider.logger.debug(
                '%s Proxy: %s' % (request, request.meta['proxy'])
            )

    @classmethod
    def from_crawler(cls, crawler):
        return cls(crawler.settings.get('HTTP_PROXIES'))


#class SpiderMiddleware(object):

#    def __init__(self, debug_mode):
#        self._active = True if debug_mode is not None else False

#    def process_spider_output(self, response, result, spider):
#        if not self._active:
#            return result

#    @classmethod
#    def from_crawler(cls, shopcrawler):
#        return cls(crawler.settings.get('DEBUG_MODE'))


class RetryStopMiddleware(object):
    def __init__(self, settings):
        self.retry_http_codes = set(int(x) for x in
                settings.getlist('STOP_RETRY_HTTP_CODES'))

    def process_response(self, request, response, spider):
        if response.status in self.retry_http_codes and not request.meta.get('stop_retry'):
            for req in spider.crawler.engine.slot.inprogress:
                if request.meta.get('stop_retry'):
                    continue
                spider.logger.debug("Reduce retry number for req %s" % req)
                req.meta['stop_retry'] = True
                req.meta['retry_times'] = 0
                req.meta['download_timeout'] += 100
            spider.logger.info('Sleep for 60 seconds and retry')
            time.sleep(60)
            req = request.copy()
            req.meta['stop_retry'] = True
            req.meta['retry_times'] = 0
            req.dont_filter = True
            req.priority = req.priority + 5
            return req
        return response

    @classmethod
    def from_crawler(cls, crawler):
        if str(crawler.settings.get('DOWNLOAD_DELAY')) != '0':
            return
        return cls(crawler.settings)


import re


def PriceStringToNumber(logger, price_string):
    '''
    This method is used by multiple classes. 
    '''
    # if priceStr[0:3] != "RP ": 
    #   logger.error('priceStr is not heading with RP.'  + (", \n priceStr: %s" %  priceStr))
    #   return -1
    # priceStr = priceStr[3:].replace(".", "")
    # if priceStr[-1] == ',':
    #   priceStr = priceStr[:-1]
    # price_arg = price_string.split(".")
    # if len(price_arg) > 2:
    #     whole_number = price_arg[0] + price_arg[1]
    #     price_string = "%s.%s" % (whole_number, price_arg[2])
    price = long(price_string.replace(".", ""))
    if price < 0:
        logger.error('price is negative! %s' % price)
        return -1
    return price


def PriceStringToNumberBukalapak(logger, price_string):
    '''
    This method is used by bukalapak spider.
    '''
    # test
    price = long(re.sub('[^0-9]+', '', price_string))
    if price < 0:
        logger.error('price is negative! %s' % price)
        return -1
    return price
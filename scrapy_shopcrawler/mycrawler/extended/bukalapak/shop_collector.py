from scrapy import Request
from mycrawler.items import ShopItem, ProductItem, MetricsItem
from mycrawler.util import Util

class ShopCollector():
  def __init__(self, inputboxPath, shopLinksOnCurrentResultPagePath, nextShopPagePath='', ithPagePath=''):
    self.inputboxPath=inputboxPath
    self.shopLinksOnCurrentResultPagePath = shopLinksOnCurrentResultPagePath
    self.nextShopPagePath = nextShopPagePath
    self.ithPagePath = ithPagePath

  def CollectShopsFromKeywords(self, logger, keywords):
    '''
    Collect shops from keywords and save the shop info into DB.
    '''
    self.logger = logger
    self.logger.debug("CollectShopsFromKeywords")
    for keyword in keywords:
      # kewword is like '4g lte modem'
      url = self.inputboxPath % '+'.join(keyword.split())
      self.logger.debug("url to search: %s" % url)
      yield Request(url, callback = self.__CrawlSellerShopsFromKeyword)

  def __CrawlSellerShopsFromKeyword(self, response):
    self.logger.debug("__CrawlSellerShopsFromKeyword")
    # record shops on the current page
    for shopUrl in response.xpath(shopLinksOnCurrentResultPagePath):
      try:
        shopItem = ShopItem()
        shopItem['title'] = shopUrl.extract()[1:]
        self.logger.debug("shopItem['title']: %s" % shopItem['title'])
        shopItem['url'] = response.urljoin(shopUrl.extract())
        self.logger.debug("shopItem['url']: %s" % shopItem['url'])
        from mycrawler.extended.bukalapak import ParseFeedback
        feedbackNumber, score = ParseFeedback(response, self.logger)
        shopItem['feedbackNumber'] = feedbackNumber
        self.logger.debug("aaa, feedbackNumber: %s" % feedbackNumber)
      except Exception as e:
        import sys
        self.logger.error('Error on line {}'.format(sys.exc_info()[-1].tb_lineno))
        self.logger.error('str(e) %s' %  str(e))
        self.logger.error(e.message)
        raise e
      yield shopItem

    # crawl shops on the next page
    if nextShopPagePath:
      nextPageUrl = Util.GetValueByXpaths(self.logger, response, nextShopPagePath)
      while(nextPageUrl != '-1'):
        self.logger.debug("nextPageUrl: %s" % nextPageUrl)
        yield Request(nextPageUrl, self.__CrawlSellerShopsFromKeyword)

    if ithPagePath:
      for i in range(100):
        ithPageUrl = Util.GetValueByXpaths(self.logger, response, ithPagePath % i)
        if ithPageUrl == '-1':
          break
        self.logger.debug("ithPageUrl: %s" % ithPageUrl)
        yield Request(ithPageUrl, self.__CrawlSellerShopsFromKeyword)

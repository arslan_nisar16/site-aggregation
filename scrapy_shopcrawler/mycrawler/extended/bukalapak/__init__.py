from mycrawler.util import Util

def ParseFeedback(response, logger):
  feedback = Util.GetValueByXpaths(logger, response, ["//a[@class='user-feedback-summary']/text()"])

  import re
  m = re.search('([0-9]+)% \(([0-9]+) feedback\)', feedback)
  assert m, 'abnormal feedback format'
  feedbackNumber = int(m.group(2)) 
  score = float(m.group(1)) / 100
  return feedbackNumber, score

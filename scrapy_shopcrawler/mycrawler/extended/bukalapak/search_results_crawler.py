from mycrawler.items import RecordProduct
import sys

class SearchResultsCrawler():
  @classmethod
  def Init(cls, logger):
    cls.logger = logger 

  @classmethod
  def RecordBukalapakProducts(cls, response):
    baseXpath = cls.GetBaseXpath(response)

    if baseXpath == "":
      cls.logger.warning("result do not exist. respnse.url: %s" % (response.url))
      return 
      yield

    cls.logger.debug("baseXpath: %s" % baseXpath)
    for i in range(1,25):
      baseXpathWithIndex =  baseXpath % i
      if cls.NoMoreResult(response, urlXpath = baseXpathWithIndex + "@data-url"):
        break

      urlXpath = baseXpathWithIndex + "@data-url"
      titleXpath = baseXpathWithIndex + "div[3]/h3/a/@title"
      imagePathXpath = baseXpathWithIndex + "div[1]/a/img/@src"
      priceXpath = baseXpathWithIndex + "div[3]/div[2]/span/span[2]/text()"

      try:
        url = cls.FormUrl(response, urlXpath)
        title = response.xpath(titleXpath)[0].extract()
        imagePath = response.xpath(imagePathXpath)[0].extract()
        price = response.xpath(priceXpath)[0].extract()
        price = price.replace('.', '')
      except Exception as e:
        cls.logger.error('Error on line {}'.format(sys.exc_info()[-1].tb_lineno))
        cls.logger.error(e.message + ",\n response.url: %s" % response.url  + (",\n urlXpath: %s" % urlXpath)  + (",\n titleXpath: %s" % titleXpath)  + (",\n priceXpath: %s" % priceXpath))
        return 
        yield

      for item in RecordProduct(cls.logger, response, url, title, imagePath, price, response.meta['data']['searchKey'], response.meta['data']['subSearchKey']):
        yield item

  @classmethod
  def GetBaseXpath(cls, response):
    '''
    Somehow, base xpath change from time to time. Have to dynamically get it.
    '''
    baseXpaths = [
        '/html/body/div[1]/div[3]/div/div[1]/div/ul/li[%s]/article/',
        '/html/body/div[1]/div[4]/div/div[1]/div/ul/li[%s]/article/', # for 'https://www.bukalapak.com/products?utf8=%E2%9C%93&search%5Bkeywords%5D=Lyra+Sketch+Book+3'
        ]

    for baseXpath in baseXpaths:
      priceXpath = baseXpath % 1 + 'div[3]/div[2]/span/span[2]/text()'
      cls.logger.debug("priceXpath: %s" % priceXpath)
      if len(response.xpath(priceXpath)):
        return baseXpath
    return ""

  @classmethod
  def NoMoreResult(cls, response, urlXpath):
    return len(response.xpath(urlXpath)) == 0

  @classmethod
  def FormUrl(cls, response, urlXpath):
    cls.logger.debug("urlXpath: %s" % urlXpath)
    url = response.xpath(urlXpath)[0].extract()
    cls.logger.debug("url: %s" % url)
    url = 'https://www.bukalapak.com' + url
    return url

import urllib
import urlparse
from mycrawler.settings import DEBUG_MODE
from scrapy import Request

from mycrawler.extended.lazada.util import PriceStringToNumber, PriceStringToNumberBukalapak
from mycrawler.itemloader import DefaultItemLoader
from mycrawler.items import MetricsItem
#from apps.products.models import Product, Shop
from products.models import Product, Shop
from mycrawler.util import Util


class EcMarketAnalyzer:
    '''
    I need to ues scrapy logger that passed to this module.
    Declear a dummy logger here to prevent pylint from complaining.
    '''
    logger = None
    xpaths = {
        'outOfStockPage': '//div[contains(@class, "content-segment") and contains(@class, "product-unavailable")]',
        'nextProductPage': '//div[@class="pagination"]//a[@rel="next"]/@href',
        'shop_title': '//h1[contains(@class, "page-title")]//a/text()',
        'product_url': '//li/div/article/div/a/@href',   # incorrect path (old) '//li[@class="product"]//h3//a/@href'
        'product_section': '//@itemscope',    # incorrect path (old) '//section[@itemscope]' #TODO fix correct section
        'product_title': ['.//h1[@itemprop="name"]/text()'],
        'product_category': ['.//dd[@itemprop="category"]/text()'],
        'product_description': ['.//div[contains(@id, "product_desc")]//p/text()'],
        'product_price': ['//span[contains(@class, "amount")]/text()'],
        #     './/span[@itemprop="price"]'
        #     '//span[contains(@class, "amount")]/text()'
        # ],
        #'product_stock': './/div[@class="product-stock"]/strong/text()',
        'product_pageviews':  ['//div[@class="product-stats"]//dd[@title="Dilihat"]/strong/text()'],
        'product_likes':  ['//div[@class="product-stats"]//dd[@title="Peminat"]/strong/text()'],
        'product_sales':  ['//div[@class="product-stats"]//dd[@title="Terjual"]/strong/text()'],
        'shopUrl': "//article[@class='user-display']//h5[@class='user__name']/a/@href",
        'shopTitle': "//article[@class='user-display']//h5[@class='user__name']/a/@title",
        #'feedbackNumber': ["//a[@class='user-feedback-summary']/text()"],
    }
    rootUrl = 'https://www.bukalapak.com'
    xhr_url = 'https://www.bukalapak.com/products/%s/user_manage'
    xhr_headers = {'X-Requested-With': 'XMLHttpRequest'}
    crawlAllShops = False

    shop_urls_to_db_ids = {}

    @classmethod
    def RecordProductsInShops(cls, logger, shopUrls):
        cls.logger = logger
        cls.logger.debug("RecordProductsInShops")
        for shopUrl in shopUrls:
            cls.logger.info('Processing shop :: %s' % shopUrl)
            yield cls.ToRecordProductsInShop(cls.logger, shopUrl)

    @classmethod
    def ToRecordProductsInShop(cls, logger, shopUrl):
        cls.logger = logger
        cls.logger.debug("ToRecordProductsInShop")
        url = shopUrl.rstrip('/') + '/products'
        return Request(
            url,
            callback=cls.__parseShopProducts,
            meta={
                'data': {
                    'shopUrl': shopUrl,
                    'page': 1,
                }
            },
        )

    @classmethod
    def ToCategoryProducts(cls, logger, url, cat_meta=None):
        meta = {"data": {'page': 1}}
        if cat_meta is not None:
            meta['data']['category_id'] = cat_meta['category_id']
        cls.logger = logger
        cls.logger.debug("ToCategoryProducts")
        params = dict(
            view_mode='grid',
            per_page=96
            )
        url_parts = list(urlparse.urlparse(url))
        query = params
        url_parts[4] = urllib.urlencode(query)
        url = urlparse.urlunparse(url_parts)
        return Request(
            url,
            callback=cls.__parseCategoryProducts,
            meta=meta
        )

    @classmethod
    def __parseCategoryProducts(cls, response):
        cls.logger.debug("__parseCategoryProducts")
        try:
            if response.meta['data']['page'] == 1:
                cls._validate_url(response, 'shop')
        except Exception:
            cls.logger.exception('')
            raise
        category_id = response.meta['data'].get('category_id')
        for request in cls.UpdateMetricsOfProductsOnCurrentShop(response, None, category_id):
            yield request

        for request in cls.__RequestNextPageOfCurrentShop(response, None):
            yield request

    @classmethod
    def __parseShopProducts(cls, response):
        cls.logger.debug("__parseShopProducts")
        try:
            if response.meta['data']['page'] == 1:
                cls._validate_url(response, 'shop')
            # shopItem = response.meta['data'].get('shopItem')
        except Exception as e:
            import sys
            cls.logger.error('Error on line {}'.format(sys.exc_info()[-1].tb_lineno))
            cls.logger.error(e.message)
            raise e
        shopReference = Shop.objects.filter(url=response.meta['data']['shopUrl'])

        # if shopItem is None:
          # shopItem = cls.LoadShopItemFromDb(response, response.meta['data']['shopUrl'])
          # cls.logger.debug("yield str(shopItem): %s" % str(shopItem))
          # yield shopItem
        # if not DEBUG_MODE and shopItem.db_entry is None:
          # cls.logger.debug("loading shopItem failed")
          # return
        # shopReference = shopItem['url'] if shopItem else shopItem.db_entry

        for request in cls.UpdateMetricsOfProductsOnCurrentShop(response, shopReference):
            yield request

        for request in cls.__RequestNextPageOfCurrentShop(response, shopReference):
            yield request

    @classmethod
    def UpdateMetricsOfProductsOnCurrentShop(cls, response, shopReference, category_id=None):
        for product_url in response.xpath(cls.xpaths['product_url']).extract():
            product_url = response.urljoin(product_url).split('?')[0]  # remove query
            cls.logger.info('processing product :: %s' % product_url)

            yield Request(
                product_url,
                callback=cls.parse_product,
                priority=1,
                meta={
                    'data': {
                        'shopReference': shopReference,
                        'category_id': category_id
                    }
                }
            )

    @classmethod
    def __RequestNextPageOfCurrentShop(cls, response, shopItem):
        cls.logger.debug("__RequestNextPageOfCurrentShop")
        next_page_url = response.xpath(cls.xpaths['nextProductPage']).extract_first()
        if next_page_url:
            yield Request(
                response.urljoin(next_page_url),
                callback=cls.__parseShopProducts,
                priority=0,
                meta={
                    'data': {
                        'shopUrl': response.meta['data'].get('shopUrl'),
                        'shopItem': shopItem,
                        'page': response.meta['data']['page'] + 1,
                        'category_id': response.meta['data'].get('category_id')
                    }
                },
            )

    @classmethod
    def parse_product(cls, response, logger=None):
        if logger is not None and not cls.logger:
            cls.logger = logger
        try:
            assert cls.logger
            cls.logger.debug("parse_product")

            productUrl = cls._validate_url(response, 'product')
            product_section = response.xpath(cls.xpaths['product_section'])

            if not product_section:
                cls.logger.error('No product section on the page :: %s. return' % productUrl)
                return

            assert 'data' in response.meta
            # assert 'shopReference' in response.meta['data']

            product = Product.objects.filter(url=productUrl).first()

            unavailable_div = response.xpath(cls.xpaths['outOfStockPage'])
            if unavailable_div:
                if product is not None:
                    product.in_stock = False
                    product.save()
                return
            if product is None:
                product = Product()
            product.in_stock = True
            product.url = productUrl

            cls.__UpdateProduct(
                response,
                product,
                shopReference=response.meta['data'].get('shopReference'),
                category_id=response.meta['data'].get('category_id')
            )
        except Exception as e:
            import sys
            cls.logger.error('Error on line {}'.format(sys.exc_info()[-1].tb_lineno))
            cls.logger.error('str(e) %s' % str(e))
            cls.logger.debug("response.url: %s" % response.url)
            cls.logger.error(e.message)
            raise e
        else:
            for request in cls.__GetMetricsItemForUpdate(response, product, product_section):
                yield request

    @classmethod
    def __UpdateProduct(cls, response, product, shopReference=None, category_id=None):
        '''
            MetricsItem need its product exists in DB before it can be inserted into DB.
            This method directly writes Product into DB instead of throwing ProductItem
            into pipelines so that MetricsItem can be immediately inserted.
        '''
        cls.logger.debug("__UpdateProduct")
        from peewee import IntegrityError
        try:
            # title returned from xpath usually contains \n and the head and tail.
            title = Util.GetValueByXpaths(cls.logger, response, cls.xpaths['product_title'])
            if title[0] == '\n':
                title = title[1:]
            if title[-1] == '\n':
                title = title[:-1]
            product.title = title[:255]

            if category_id is not None:
                product.category_id = category_id
            product.description = Util.GetValueByXpaths(cls.logger, response, cls.xpaths['product_description'])[:255]
            shop_url = response.xpath(cls.xpaths['shopUrl']).extract_first()
            assert shop_url
            shop_url = response.urljoin(shop_url)
            shop_id = cls.shop_urls_to_db_ids.get(shop_url)
            if shop_id is None:
                shop = Shop.objects.filter(url=shop_url).first()
                if not shop:
                    shop = Shop()
                shop.url = shop_url
                shop_title = response.xpath(cls.xpaths['shopTitle']).extract_first()
                shop.title = shop_title
                shop.save()
                cls.logger.info(u"Updated shop '{}' with url {}".format(shop_title, shop_url))
                shop_id = shop.id
                cls.shop_urls_to_db_ids[shop_url] = shop_id
            product.shop_id = shop_id

            if not DEBUG_MODE:
                product.save()
            return product
        except IntegrityError, message:
            errorcode = message[0]  # get MySQL error code
            if errorcode == 1062:  # if duplicate
                pass
        except Exception as e:
            import sys
            cls.logger.error('Error on line {}'.format(sys.exc_info()[-1].tb_lineno))
            cls.logger.error('DBError :: %s' % str(e))
            cls.logger.debug("productUrl: %s" % product.url)
            cls.logger.error(e.message)
            raise e

    @classmethod
    def __GetMetricsItemForUpdate(cls, response, productReference, product_section):
        cls.logger.debug("__GetMetricsItemForUpdate")

        try:
            metrics_item = DefaultItemLoader(MetricsItem(), selector=product_section[0])
            price = Util.GetValueByXpaths(cls.logger, response,  cls.xpaths['product_price'])
            if price == '-1':
                price = 0
            price = PriceStringToNumberBukalapak(cls.logger, price)
            if price == -1:
                price = 0
            metrics_item.add_value('price', str(price))
            metrics_item.add_value('product', productReference)
            productUrl = cls._validate_url(response, 'product')
            metrics_url = cls.xhr_url % productUrl.rstrip('/').split('/')[-1]
            cls.logger.debug("yield Request metrics_item: %s" % metrics_url)
        except Exception as e:
            import sys
            cls.logger.error('Error on line {}'.format(sys.exc_info()[-1].tb_lineno))
            cls.logger.error('str(e) %s' % str(e))
            cls.logger.error(e.message)
            raise e
        else:
            yield Request(
                metrics_url,
                callback=cls.__parse_RecordMetrics,
                priority=2,
                headers=cls.xhr_headers,
                meta={
                    'data': {'metrics_item': metrics_item},
                },
            )

    @classmethod
    def _validate_url(cls, response, obj=''):
        # check if the real URL of loaded page differs from the requested one
        cls.logger.debug("_validate_url")

        request_url = response.request.url.split('?')[0]
        response_url = response.url.split('?')[0]
        if request_url != response_url:
            cls.logger.warning(
                '%s URL mismatch :: %s != %s' %
                (obj.title(), request_url, response_url)
            )
        return response_url

    @classmethod
    def __parse_RecordMetrics(cls, response):
        cls.logger.debug("__parse_RecordMetrics" + (", response.url: %s" % response.url))
        try:
            metrics_item = response.meta['data']['metrics_item']
            sales = Util.GetValueByXpaths(cls.logger, response,  cls.xpaths['product_sales'])
            if sales == "-1" or sales < 0:
                sales = 0

            likes = Util.GetValueByXpaths(cls.logger, response,  cls.xpaths['product_likes'])
            if likes == "-1" or likes < 0:
                likes = 0

            pageviews = Util.GetValueByXpaths(cls.logger, response,  cls.xpaths['product_pageviews'])
            if pageviews == "-1" or pageviews < 0:
                pageviews = 0

            metrics_item.add_value('likes', likes)
            metrics_item.add_value('pageviews', pageviews)
            metrics_item.add_value('sales', sales)
            metrics_item = metrics_item.load_item()
            cls.logger.debug("str(metrics_item): %s" % str(metrics_item))
        except Exception as e:
            import sys
            cls.logger.error('Error on line {}'.format(sys.exc_info()[-1].tb_lineno))
            cls.logger.error(e.message)
            raise e
        else:
            yield metrics_item

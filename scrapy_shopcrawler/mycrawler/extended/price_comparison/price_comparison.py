# -*- coding: utf-8 -*-
#from apps.products.models import Keywords
from products.models import Keywords
from mycrawler.items import *
from mycrawler.items import RecordProduct
from scrapy import Request
from mycrawler.itemloader import DefaultItemLoader
from mycrawler.extended.bukalapak.search_results_crawler import SearchResultsCrawler as BukalapakCrawler
from mycrawler.extended.lazada.search_results_crawler import SearchResultsCrawler as LazadaCrawler
import sys

import logging.config
#logging.config.fileConfig('/home/xling/base/documents/scripts/crossplatform/python/logging.conf')
import logging
#logger = logging.getLogger(__name__)
    
class PriceComparison():
  '''
  Let title = response.meta['data']['title']
  Later, we do 
    metrics = Metrics.select(Metrics.price).join(Product).join(SearchKeys).where(SearchKeys.searchKey == title, Product.url.contains('product'))
    prices = [metric.price for metric in metrics]
  '''

  @classmethod
  def ComparePrice(cls, logger):
    cls.logger = logger
    seedKeywordNumber = 30# default: 100
    keywords = Keywords.select().order_by((Keywords.searchVolume / Keywords.productNumber).desc()).limit(seedKeywordNumber)
    for keyword in keywords:
      cls.logger.debug("keyword.keyword: %s" % keyword.keyword)
      url = 'http://www.lazada.co.id/catalog/?q=' + '+'.join(keyword.keyword.split())
      yield Request(
        url,
        callback=cls.RecordProducts
      )

  @classmethod
  def RecordProducts(cls, response):
    # save L products
    for item in cls.RecordFirstLazadaSearchResult(response):
      yield item

  @classmethod
  def RecordFirstLazadaSearchResult(cls, response):
    url = LazadaCrawler.GetUrl(response, cls.logger)
    cls.logger.debug("lazada url: %s" % url  + (", \nresponse.url: %s" % response.url))
    if url == "":
      cls.logger.warning("search result does not exist. " + "response.url: %s" % response.url)
      # http://stackoverflow.com/questions/13243766/python-empty-generator-function
      return 
      yield

    try:
      title = LazadaCrawler.GetTitle(response, cls.logger)
      if title == "":
        cls.logger.warning("abnormal title. " + "response.url: %s" % response.url)
        return 
        yield
    except Exception as e:
      cls.logger.error('Error on line {}'.format(sys.exc_info()[-1].tb_lineno))
      cls.logger.error(e.message)
      return 
      yield

    imagePath = LazadaCrawler.GetImagePath(response, cls.logger)
    cls.logger.debug("imagePath: %s" % imagePath  + (", \nresponse.url: %s" % response.url))
    price = LazadaCrawler.GetPrice(response, cls.logger)
    searchKey = title

    for item in RecordProduct(cls.logger, response, url, title, imagePath, price, searchKey, searchKey):
      yield item
      cls.logger.debug("yield item:" + str(item))

  @classmethod
  def RecordBukalapakProducts(cls, title):
    def GetLongestSubTitle(title):
      subTitles = title.split('-')
      ret = subTitles[0]
      for subTitle in subTitles:
        if len(subTitle) > len(ret):
          ret = subTitle
      return ret

    BukalapakCrawler.Init(cls.logger)

    for request in cls.FormBukalapakRequest(title, title):
      yield request

    # some titles are like 'Lyra Sketch Book 3 - 30 Lembar'
    # B cannot find the full name but the sub name before '-'
    if '-' not in title:
      return 
      yield

    subTitles = title.split('-')
    #subSearchKey = GetLongestSubTitle(title)
    for subTitle in subTitles:
      for request in cls.FormBukalapakRequest(subSearchKey = subTitle, searchKey = title):
        cls.logger.debug("str(request): %s" % str(request))
        yield request

  @classmethod
  def FormBukalapakRequest(cls, subSearchKey, searchKey):
    cls.logger.debug("FormBukalapakRequest")
    
    def FormUrl(searchKey):
      url = 'https://www.bukalapak.com/products?utf8=%E2%9C%93&search%5Bkeywords%5D='
      url += '+'.join(searchKey.split())
      return url

    cls.logger.debug("searchKey: %s, subSearchKey: %s" % (subSearchKey, subSearchKey))

    url = FormUrl(subSearchKey)
    cls.logger.debug("url: %s" % url)
    
    yield Request(
      url,
      callback = BukalapakCrawler.RecordBukalapakProducts,
      meta={ 
        'data': { 
          'searchKey': searchKey,
          'subSearchKey': subSearchKey
          } 
        },
    )


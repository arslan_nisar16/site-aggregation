class Util():
  @classmethod
  def GetValueByXpaths(cls, logger, response, xpaths):
    try:
      assert type(xpaths) == type([]), 'xpaths should be a list'
      for xpath in xpaths:
        if len(response.xpath(xpath)):
          return response.xpath(xpath)[0].extract()
        else:
          continue
      else:
        # logger.warning("cannot parse the xpath. response.url: %s" % response.url  + (", \n xpaths: %s" %  xpaths))
        return '-1'
    except Exception as e:
      import sys
      logger.error('Error on line {}'.format(sys.exc_info()[-1].tb_lineno))
      logger.error('str(e) %s' %  str(e))
      logger.debug("response.url: %s" % response.url)
      logger.debug("xpaths: %s" % xpaths)
      logger.error(e.message)
      raise e

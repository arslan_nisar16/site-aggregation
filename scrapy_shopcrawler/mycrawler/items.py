# -*- coding: utf-8 -*-
import sys
import scrapy

from scrapy_djangoitem import DjangoItem

from products.models import (
    Keywords,
    Metrics,
    Product,
    SearchKey,
    Shop,
)

from . itemloader import DefaultItemLoader


# input filter (processor)
def is_price(value):
    return is_digit(
        map(lambda v: v.replace('.', '', 1), value)
    )


def is_digit(value):
    return filter(lambda v: v.isdigit(), value)


class ShopItem(DjangoItem):
    django_model = Shop


class ProductItem(DjangoItem):
    django_model = Product

    # following fields are listed in item.errors dict
    # when an item is processed in pipelines.py
    status = scrapy.Field(default=None)
    shop = scrapy.Field(default=None)

    #updated = 
    #internalId = 
    #last_google_image_scraped_time =
    #sellingUrls =

    def save(self, commit=True):
        if commit:
            self.instance.save()
        return self.instance


# timestamps will be saved in UTC time (GMT+0)
# to save all timestamps in local time (server's timezone)
# change 'utcnow' function to 'now' (datetime.now)
class MetricsItem(DjangoItem):
    django_model = Metrics


class KeywordItem(DjangoItem):
    django_model = Keywords


class SearchKeyItem(DjangoItem):
    django_model = SearchKey


def RecordProduct(logger, response, url, title, imagePath, price, searchKey, subSearchKey):
    logger.debug("RecordProduct")

    # record the product itself
    try:
        productItem = DefaultItemLoader(ProductItem(), response=response)
        productItem.add_value('url', url)
        productItem.add_value('title', title)
        productItem.add_value('imagePath', imagePath)
        productItem = productItem.load_item()
        logger.debug("yield productItem: %s" % str(productItem))
    except Exception as e:
        logger.error('Error on line {}'.format(sys.exc_info()[-1].tb_lineno))
        logger.error(e.message)
        raise e
    yield productItem

    # record price of the first returned product
    try:
        metricsItem = DefaultItemLoader(MetricsItem(), selector=response)
        # price must be string
        metricsItem.add_value('price', str(price))
        '''
        If productItem has an url that has existed in db. productItem will be
        automatically replaced with the productItem existing in DB.
        '''
        metricsItem.add_value('product', productItem.db_entry)
        metricsItem = metricsItem.load_item()
        logger.debug("yield metricsItem: %s" % str(metricsItem))
    except Exception as e:
        logger.error('Error on line {}'.format(sys.exc_info()[-1].tb_lineno))
        logger.error(e.message)
        raise e
    yield metricsItem

    # TODO figure out WTF is it (3 following lines)
    # record SearckKeyItem for the lazada product
    if SearchKey.objects.filter(SearchKey.searchKey == searchKey, SearchKey.product == productItem.db_entry).count() > 0:
        return
        yield

    try:
        searchKeyItem = DefaultItemLoader(SearchKeyItem(), response=response)
        searchKeyItem.add_value('searchKey', searchKey)
        searchKeyItem.add_value('subSearchKey', subSearchKey)
        searchKeyItem.add_value('product', productItem.db_entry)
        searchKeyItem = searchKeyItem.load_item()
        logger.debug("yield searchKeyItem: %s" % str(searchKeyItem))
    except Exception as e:
        logger.error('Error on line {}'.format(sys.exc_info()[-1].tb_lineno))
        logger.error(e.message)
        raise e
    yield searchKeyItem

# -*- coding: utf-8 -*-

import os
import sys

#TODO do not set DJANGO_SETTINGS_MODULE directly
#os.environ['DJANGO_SETTINGS_MODULE'] = os.environ.get('DJANGO_SETTINGS_MODULE', 'goodobtain.settings')

os.environ['DJANGO_SETTINGS_MODULE'] = 'goodobtain.settings'
os.environ['SCRAPY_SETTINGS_MODULE'] = 'scrapy_shopcrawler.mycrawler.settings'

# Project name
BOT_NAME = 'crawler'
# List of bukalapak.com shops that will be processed
# if some shop cannot be found in DB, then its entry will be created
# shop's title will be extracted from its page and saved into DB automatically
# * use 'https' to avoid unnecessary redirects from 'http'
ShopUrls = []


# Enable/disable debugging mode
DEBUG_MODE = False

# Project's root directory
SCRAPY_DIR = os.path.abspath(os.path.dirname(os.path.dirname(__file__)))
BASE_DIR = os.path.abspath(os.path.dirname(SCRAPY_DIR))
sys.path.append(BASE_DIR)

APPS_DIR = os.path.join(BASE_DIR, 'apps')
sys.path.insert(0, APPS_DIR)

# log
LOG_ENABLED = True
LOG_STDOUT = False
if not DEBUG_MODE:
    LOG_DIR = os.path.join(SCRAPY_DIR, 'log')
    if not os.path.exists(LOG_DIR):
        os.mkdir(LOG_DIR)
    LOG_FILE = os.path.join(LOG_DIR, 'crawler.log')
LOG_LEVEL = 'DEBUG'

# Telnet console
TELNETCONSOLE_ENABLED = True
TELNETCONSOLE_PORT = [6023, 6073]

# Enable scrapy-jsonrpc
# scrapy-jsonrpc is an extension to control a running Scrapy web crawler via JSON-RPC.
# The service provides access to the main Crawler object via the JSON-RPC 2.0 protocol.
JSONRPC_ENABLED = False
JSONRPC_PORT = [6080, ]

# Spiders
SPIDER_MODULES = ['mycrawler.spiders']
NEWSPIDER_MODULE = 'mycrawler.spiders'

  ## Rules (if zero, ignore the rule):
  ## Close the spider if it remains open for more than that number of seconds
CLOSESPIDER_TIMEOUT = 0
  ## Close the spider if it scrapes more than that amount if items
CLOSESPIDER_ITEMCOUNT = 0
  ## Close the spider if it crawles more than that number of pages (HTTP responses)
CLOSESPIDER_PAGECOUNT = 0
  ## Close the spider if it receives more than that number of errors
CLOSESPIDER_ERRORCOUNT = 0


  ## The amount of time (in secs) that the downloader will wait before timing out
DOWNLOAD_TIMEOUT = 45
  ## The amount of time (in secs) that the downloader should wait
  ## before downloading consecutive pages from the same website
  ## if you have download delay set to non-zero value, then crawler will work only in single thread,
  ## no matter how many concurrent requests you set above so change it to zero or just comment this line
  ## When it is too small, MetricsItems cannot be correctly obtained (buka will bans you).
DOWNLOAD_DELAY = 1
  ## Wait a random amount of time (between 0.5 and 1.5 * DOWNLOAD_DELAY)
  ## Does not matter if DOWNLOAD_DELAY is zero.
RANDOMIZE_DOWNLOAD_DELAY = True

  ## Configure maximum concurrent requests performed by Scrapy (default: 16)
CONCURRENT_REQUESTS = 30
  ## The maximum number of concurrent requests per domain
CONCURRENT_REQUESTS_PER_DOMAIN = 30
  ## The maximum number of concurrent request per IP
  ## If non-zero, the CONCURRENT_REQUESTS_PER_DOMAIN setting is ignored
  ## and download delay is enforced per IP, not per domain
CONCURRENT_REQUESTS_PER_IP = 0

  ## HTTP redirect
REDIRECT_ENABLED = True

  ## Max number of allowed redirects
REDIRECT_MAX_TIMES = 5
REDIRECT_PRIORITY_ADJUST = 2
  ## META redirect
METAREFRESH_ENABLED = True
METAREFRESH_MAXDELAY = 15

  ## Retry failed requests
RETRY_ENABLED = True
RETRY_TIMES = 15
RETRY_PRIORITY_ADJUST = 0
  ## too many requests responses
STOP_RETRY_HTTP_CODES = [429, 403]
RETRY_HTTP_CODES = [500, 502, 503, 504, 408] + STOP_RETRY_HTTP_CODES
# RETRY_HTTP_CODES += TOO_MANY_REQUESTS_CODES
# RETRY_TIMES = 25
  ## Populates 'Referer' header by using URL of the previos Response
REFERER_ENABLED = True
  ## AJAX
AJAXCRAWL_ENABLED = False

  ## The maximum depth that will be allowed to crawl for any site
  ## if zero, no limit will be imposed
DEPTH_LIMIT = 0

# DNS
  # DNS in-memory cache
DNSCACHE_ENABLED = True
DNSCACHE_SIZE = 10000
  # Timeout for processing of DNS queries in seconds
DNS_TIMEOUT = 30
# Respect robots.txt policies
ROBOTSTXT_OBEY = False

# Proxy settings
PROXY_ENABLED = False
  # List of HTTP proxies
HTTP_PROXIES = ['http://127.0.0.1:8123', ]

# HTTP request headers
DEFAULT_REQUEST_HEADERS = {
    'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
    'Accept-Language': 'en',
}

## If we don't use proxies, then it has no sense to rotate User-Agent header
if not PROXY_ENABLED:
    USER_AGENT = 'Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:22.0) Gecko/20100101 Firefox/22.0'

## Cookies
COOKIES_ENABLED = False
COOKIES_DEBUG = True if DEBUG_MODE else False

  # Enable and configure the AutoThrottle extension (disabled by default)
  # NOTE: AutoThrottle will honour the standard settings for concurrency and delay
  #AUTOTHROTTLE_ENABLED=True
  # The initial download delay
  #AUTOTHROTTLE_START_DELAY=5
  # The maximum download delay to be set in case of high latencies
  #AUTOTHROTTLE_MAX_DELAY=60
  # Enable showing throttling stats for every response received:
  #AUTOTHROTTLE_DEBUG=False

# memory

## Memory settings:

## Memory debugging
MEMDEBUG_ENABLED = True
  # Enable the memory usage extension that will shutdown the Scrapy process
  # when it exceeds a memory limit (useful when lxml causes memory leaks)
MEMUSAGE_ENABLED = False
  # The maximum amount of memory to allow (in megabytes)
  # before shutting down the Scrapy
  # If zero, no check will be performed
MEMUSAGE_LIMIT_MB = 0

  # Maximum number of concurrent items (per response)
  # to process in parallel in the Item Pipeline
CONCURRENT_ITEMS = 100

  # The maximum limit for Twisted Reactor thread pool size
REACTOR_THREADPOOL_MAXSIZE = 10

# Item pipelines
ITEM_PIPELINES = {
    # default pipeline
    #'crawler.pipelines.DefaultItemPipeline': 100,
    # DB pipeline that saves items into MySQL database
    # * disabled in DEBUG_MODE
    'mycrawler.pipelines.MySQLPipeline': 200 if not DEBUG_MODE else None,
}

# Downloader Middlewares
DOWNLOADER_MIDDLEWARES = {
    # default user-agent middleware
    # * disabled if we use proxies and random user-agents
    'scrapy.downloadermiddlewares.useragent.UserAgentMiddleware': (
        400 if not PROXY_ENABLED else None
    ),
    # random user-agent middleware
    # * enabled only if we use proxies
    'crawler.middlewares.RotateUserAgentMiddleware': (
        400 if PROXY_ENABLED else None
    ),
    # proxy middleware
    # * enabled if PROXY_ENABLED and HTTP_PROXIES list is not empty
    'crawler.middlewares.ProxyMiddleware': (
        740 if PROXY_ENABLED and HTTP_PROXIES else None
    ),
    'scrapy.downloadermiddlewares.httpproxy.HttpProxyMiddleware': 300,
    # before retry middleware; after all retries stop crawler for 1 minute and
    # try again
    'mycrawler.middlewares.RetryStopMiddleware': 490
}

# Spider Middlewares
#SPIDER_MIDDLEWARES = {}

# Extensions

RUNCOMMANDS_EXT_ENABLED = True
RUNCOMMANDS_EXT_COMMANDS = {
    'ExistingBukalapakSpider': '/var/www/shop_crawler/venv/bin/python '
                               '/var/www/python-scrapy-data-mining/get_clusters.py'
}

EXTENSIONS = {
    'mycrawler.extensions.RunCommandExtension': 0,
    'scrapy.extensions.corestats.CoreStats': 500,
    'scrapy.extensions.spiderstate.SpiderState': 500,
    'scrapy_jsonrpc.webservice.WebService': 500,
}

# Private settings (sensitive user data)
try:
    # 'private_settings.py' module exists
    from private_settings import *
except ImportError:
    # 'private_settings.py' module doesn't exist
    pass

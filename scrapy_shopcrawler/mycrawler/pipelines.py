# -*- coding: utf-8 -*-

import logging
import coloredlogs

from products.models import Product

logger = logging.getLogger('your-module')
coloredlogs.install(level='DEBUG')


class MySQLPipeline(object):
    def process_item(self, item, spider):
        try:
            logger.debug("process_item: type(item): %s" % type(item))
            logger.debug("process_item: str(item): %s" % str(item))
            if issubclass(item.django_model, Product):
                # some additional validations for Product items
                if not item.get('price'):
                    # ignore product that is currently unavailable
                    # for example, price=0 or is not shown on the web site
                    return

            item.save()
            # Update assignment of 'weight' later on when sales and competitorNumber is available
            # if item.__class__.__name__ == "MetricsItem":
            #     logger.debug("process_product: str(item): %s" % str(item["product"]))
            #     product = item["product"]
            #     pageviews = int(item["pageviews"])
            #     price = float(item["price"])
            #     product.weight = pageviews / price
            #     product.save()
        except Exception as e:
            if 'duplicate' in e.message:
                return
            logger.error("e.message: %s" % e.message)
            logger.error("e.__cause__.pgcode: %s" % e.__cause__.pgcode)
            raise e

/* Project specific Javascript goes here. */

// http://vitalets.github.io/x-editable/docs.html 
$.fn.editable.defaults.mode = 'popup';

$(document).ready(function() {
    $('.js-manual-similar-products').editable({
        success: function(response, newValue) {
            console.log(response.location);
            if(!response.success) return response.message;
        },
        error: function(response, newValue) {
           console.log(response);
           if(response.status === 500) {
                return 'Service unavailable. Please try later.';
            } else {
                return response.responseText;
            }
        }
    });
});

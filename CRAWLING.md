## Crawling

### Control the crawling scope

#### Add new categories

New categories can be added/changed via [admin](http://goodobtain.com/admin/products/category/) panel.

Active categories are crawled automatically.


#### Enable / disable categories

NOTICE: Disabled categories **won't** be crawled.

![enable categoy](http://i.imgur.com/1Himlz7.png)


### Launching the crawlers

#### Launching single crawler

Log into server as 'goodobtain' user and execute following commands:

    cd /home/goodobtain/site-aggregation/

    source venv/bin/activate

    cd scrapy_shopcrawler/


Run needed spider. e.g.:

    # crawl ALL ACTIVE tokopedia categories
    scrapy crawl tokopedia_all_products

or

    # crawl(update) ONLY tokopedia products that are already in the DB
    scrapy crawl tokopedia_existing_products_only


Get list of available spiders:

    scrapy list


#### Launching crawlers by schedule

[Crontab task](conf/crontab?at=master&fileviewer=file-view-default) is used for this purpose.
Cron executes shell script [run_scrapy.sh](bin/run_scrapy.sh)

To start crawlers manualy execute(as goodobtain user):

    $HOME/site-aggregation/bin/run_scrapy.sh


### Excluding less important products.

To accelerate the crawling, we exclue less important products: 

* price < 80000
* price > 500000
* sales_rate < 0.6


### Avoid being banned
[bukalapak.com](bukalapak.com) indeed bans proxy servers every 2 minutes(responses with codes 403 and 429). There is middleware to solve it that stop crawler for a minute and then retry requests.)

The problem indeed with crawler blocking: bukalapak server blocks a crawler's ip with 429 HTTP error( which means too many requests) for 60 seconds.

Solution: to stop the crawler for a minute and retry all failed requests.
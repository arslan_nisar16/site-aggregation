# INSTALL tutorial to DigitalOcean

Following steps were performed while moving to new droplet. With used serverpilot.

## First install:

    sudo apt-get update
    sudo apt-get install mc htop

## Python 2.7 install:

    sudo apt-get install build-essential checkinstall
    sudo apt-get install libreadline-gplv2-dev libncursesw5-dev libssl-dev libsqlite3-dev tk-dev libgdbm-dev libc6-dev libbz2-dev
    mkdir temp
    cd temp
    wget http://python.org/ftp/python/2.7.12/Python-2.7.12.tgz
    tar -xvf Python-2.7.12.tgz
    cd Python-2.7.12
    ./configure
    make
    sudo checkinstall
    sudo pip install --upgrade pip
    pip3 install --upgrade pip

## PostgreSQL 2.7 install:

    sudo apt-get install postgresql postgresql-contrib
    
    sudo -u postgres psql
    create database database_name;
    CREATE USER database_user WITH password 'database_password';
    GRANT ALL privileges ON DATABASE database_name TO database_user;
    \q

## Other soft install:

    sudo apt-get install python-virtualenv libjpeg-dev zlib1g-dev libmysqlclient-dev python-dev libssl-dev \
        libxml2-dev libxslt-dev libghc-zlib-dev checkinstall \
        python-virtualenv python-psycopg2 libpq-dev

## Project install

    cd ../../
    git clone git clone https://syschel@bitbucket.org/leonexu/site-aggregation.git
    cd site-aggregation
    virtualenv venv
    source venv/bin/activate
    pip install -r requirements.txt
    
    # add Django info in database
    python manage.py migrate
    
    # create template files
    python manage.py collectstatic
    
    # add Super User Django in database(admin)
    python manage.py createsuperuser
    
    # add test data in database
    python manage.py loaddata apps/products/fixtures/website.json
    python manage.py loaddata apps/products/fixtures/category.json

## Configure Nginx and Supervisor

    sudo apt-get install supervisor
    ln -s /home/goodobtain/site-aggregation/conf/goodobtain.supervisor.conf /etc/supervisor/conf.d/goodobtain.supervisor.conf 
    sudo service supervisor restart
    
    sudo ln -s /home/goodobtain/site-aggregation/conf/goodobtain.nginx.conf /etc/nginx-sp/vhosts.d/
    sudo service nginx-sp reload
    
    
    
    
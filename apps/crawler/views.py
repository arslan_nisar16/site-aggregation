#from django.shortcuts import render
import urllib
import json

from django.views.generic import TemplateView

from braces.views import StaffuserRequiredMixin
from scrapy_jsonrpc.jsonrpc import jsonrpc_client_call, JsonRpcError


class CrawlerListView(StaffuserRequiredMixin, TemplateView):
    template_name = "crawler/crawler_list.html"
    #TODO get this value from scrapy settings
    scrapy_webservice_url = 'http://localhost:6080'

    open_spiders_url = '/crawler/engine/open_spiders'
    available_spiders_url ='/crawler/spiders'


    def get_open_spiders(self):
        try:
            return json.loads(urllib.urlopen(self.scrapy_webservice_url+self.open_spiders_url).read())
        except IOError:
            return []

    def get_available_spiders(self):
        try:
            return jsonrpc_client_call(
                '%s%s' % (self.scrapy_webservice_url, self.available_spiders_url),
                'list'
            )
        except Exception as ex:
            print(ex)
            return []

    def get_context_data(self, **kwargs):
        context_data = super(CrawlerListView, self).get_context_data(**kwargs)
        context_data['open_spider_list'] = self.get_open_spiders()
        context_data['available_spider_list'] = self.get_available_spiders()
        return context_data

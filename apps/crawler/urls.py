#-*- coding: utf-8 -*-
from django.conf.urls import url
#from django.contrib.auth.decorators import login_required

from . views import CrawlerListView


urlpatterns = [
    url(r'^$', CrawlerListView.as_view(), name='crawler_list'),
    #url(r'^detail/(?P<pk>.*\d+)/$', CrawlerDetailView.as_view(), name='crawler_detail'),
]

import ipdb

from django.core.management.base import BaseCommand
from django.conf import settings
from django.utils.crypto import get_random_string

from products.utils import (
    get_unread_messages,
    parse_order_message,
    is_new_order_message,
)

from products.models import OwnProduct, Sale, Website

SALE_EMAIL_USER = getattr(settings, 'SALE_EMAIL_USER', '')
SALE_EMAIL_PASSWORD = getattr(settings, 'SALE_EMAIL_PASSWORD', '')


class Command(BaseCommand):
    help = 'Updates selling speed'

    def build_query_dict(self, order_dict):
        """Builds filter query dict to find product"""
        filter_dict = {}
        product_url = order_dict.get('product_url')
        product_title = order_dict.get('product_title')
        if product_url:
            filter_dict.update({'url': product_url})
        elif product_title:
            filter_dict.update({'title': product_title})
        return filter_dict

    def build_new_product_dict(self, order_dict):
        """Builds dict for new product creation"""
        new_product_dict = {}

        product_title = order_dict.get('product_title')
        if product_title:
            new_product_dict.update({'title': product_title})

        product_url = order_dict.get('product_url')
        if product_url:
            new_product_dict.update({'url': product_url})
        else:
            # url field can't be empty due to IntegrityError: UNIQUE constraint failed: products_ownproduct.url
            # so fill it out with some random value
            new_product_dict.update({'url': get_random_string()})

        website = Website.objects.get(base_url__contains=order_dict.get('website'))
        new_product_dict.update({'website_id': website.id})
        return new_product_dict

    def handle(self, *args, **options):
        mail_messages_iterator = get_unread_messages(SALE_EMAIL_USER, SALE_EMAIL_PASSWORD)

        for message in mail_messages_iterator:
            print('Subject: "%s"' % message.headers['Subject'])
            if not(is_new_order_message(message)):
                # if message does not contain 'New order'
                # then mark it as 'READ' and continue
                message.read()
                continue

            try:
                order_dict = parse_order_message(message)
                print(order_dict)
            except Exception as ex:
                print 'Parsing order message error:', ex
                continue

            try:
                filter_dict = self.build_query_dict(order_dict)
                print "filter_dict", filter_dict
                own_product = OwnProduct.objects.get(**filter_dict)
            except OwnProduct.DoesNotExist as ex:
                print "own_product DoesNotExist", ex
                # create new product
                new_product_dict = self.build_new_product_dict(order_dict)
                print "new_product_dict", new_product_dict
                own_product = OwnProduct.objects.create(**new_product_dict)

            try:
                Sale.objects.create(
                    product=own_product,
                    amount=order_dict['amount'],
                    transaction_datetime=order_dict['transaction_datetime'],
                )
                # mark message as 'read'
                message.read()
            except Exception as ex:
                print ex
                continue

            print '=' * 10

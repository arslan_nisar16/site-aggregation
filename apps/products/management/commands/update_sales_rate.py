import progressbar
from django.core.management.base import BaseCommand
from products.models import Product
#import time


class Command(BaseCommand):
    help = 'Update sales_rate'

    def handle(self, *args, **options):
        product_count = Product.objects.count()
        updated_count = 0

        # Due to "Segmentation fault (core dumped)"
        # Updates products by portions
        # number of products to updated per iteration
        product_bunch = 5000
        product_start_range = 0

        with progressbar.ProgressBar(max_value=product_count) as bar:
            while updated_count <= product_count:
                #print("product start range: %s" % product_start_range)
                #print("product end range: %s" % (product_start_range+product_bunch))
                for product in Product.objects.all()[product_start_range:product_start_range+product_bunch]:
                    product.update_sales_rate()
                    product.save(update_fields=['sales_rate', ])
                    updated_count += 1
                    bar.update(updated_count)
                product_start_range += product_bunch
                #time.sleep(0.5)

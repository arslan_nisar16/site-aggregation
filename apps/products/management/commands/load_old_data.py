import os
import sys

from django.conf import settings
from django.core.management import call_command
from django.core.management.base import BaseCommand, CommandError
from customers.models import Client


class Command(BaseCommand):
    help = 'Handle the loading of old database'

    def load_fixtures(self, fixture_name, schema_name="tenant1"):
        try:
            tenant = Client.objects.get(schema_name=schema_name)
            tenant.activate()

            for fixture_dir in settings.FIXTURE_DIRS:
                fixture = os.path.join(fixture_dir, fixture_name)
                if os.path.isfile(fixture):
                    call_command("loaddata", fixture)
        except Exception, e:
            raise CommandError(e.message)

    def handle(self, *args, **options):
        
        fixtures = [
            "productstatus.json",
            "products.json",
            "providers.json",
            "orders.json"
        ]
        for fixture in fixtures:
            self.load_fixtures(fixture)
        self.stdout.write('Successfully loaded data from old database.')

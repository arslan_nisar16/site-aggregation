import os
import datetime
import requests
from django.test import TestCase
import mock
from mock import MagicMock
from gmail.message import Message

from .. utils import (
    parse_bukalapak_order_message,
    parse_tokopedia_order_message,
    is_new_order_message,
)


class GoogleMailUtilsTestCase(TestCase):

    """Tests products.utils.google_mail functions"""

    def setUp(self):
        # self.client = Client()
        # User.objects.create_superuser('test', 'test@email.com', 'password')
        # self.client.login(username='test', password='password')
        pass

    def tearDown(self):
        # Product.objects.all().delete()
        pass

    def mock_gmail_message(self, message_html_file, **kwargs):
        dtime = kwargs.get('datetime') or datetime.datetime(2016, 2, 16, 23, 30)
        #dtime = kwargs.get('datetime') or datetime.datetime(2016, 2, 16, 23, 30)
        gmail_message = MagicMock(spec=Message, **kwargs)
        gmail_message.html = open(
            os.path.join(os.path.dirname(__file__), message_html_file)).read()
        gmail_message.headers = {
            'Date': datetime.datetime.strftime(dtime, "%Y-%m-%d %H:%M")
        }
        return gmail_message

    @mock.patch('requests.head')
    def test_parse_bukalapak_new_order_message(self, mock_requests_head):
        """Tests Bukalapak products.utils.google_mail.parse_bukalapak_new_order_message function"""
        mock_requests_head.return_value = MagicMock(
            spec=requests.head,
            headers={'Location': 'http://test.com'}
        )
        message_datetime = datetime.datetime(2016, 2, 16, 23, 30)
        gmail_message = self.mock_gmail_message(
            'test_bukalapak_new_order.html',
            datetime=message_datetime,
        )
        new_order_dict = parse_bukalapak_order_message(gmail_message)
        self.assertIn('amount', new_order_dict)
        self.assertIn('product_url', new_order_dict)
        self.assertIn('transaction_datetime', new_order_dict)
        self.assertIn('website', new_order_dict)
        self.assertEqual(
            {
                'amount': 1,
                'product_url': 'http://test.com',
                'transaction_datetime': message_datetime,
                'website': 'bukalapak.com',
            },
            new_order_dict
        )

    @mock.patch('requests.head')
    def test_parse_tokopedia_new_order_message(self, mock_requests_head):
        """Tests Tokopedia parse_tokopedia_new_order_message function"""
        mock_requests_head.return_value = MagicMock(
            spec=requests.head,
            headers={'Location': 'http://test.com'}
        )
        message_datetime = datetime.datetime(2016, 2, 16, 23, 30)
        gmail_message = self.mock_gmail_message(
            'test_tokopedia_new_order.html',
            datetime=message_datetime,
        )

        new_order_dict = parse_tokopedia_order_message(gmail_message)
        self.assertIn('amount', new_order_dict)
        self.assertIn('transaction_datetime', new_order_dict)
        self.assertIn('product_title', new_order_dict)
        self.assertIn('website', new_order_dict)
        self.assertEqual(
            {
                'amount': 3,
                'product_title': u'DDR2 Kingston 2Gb, Desktop ValueRAM Memory KVR800D2N6/2G (AMD only)',
                'transaction_datetime': message_datetime,
                'website': 'tokopedia.com',
            },
            new_order_dict
        )

    @mock.patch('requests.head')
    def test_is_new_order_message(self, mock_requests_head):
        """Tests is_new_order_message function
        email message with new order should contains 'Pesanan baru'  
        it's 'New order' in Indonesian
        """
        new_order_gmail_message = MagicMock(
            spec=Message,
            subject='Fwd: Pesanan Baru! Segera kirim dan masukkan nomor resi',
        )
        self.assertTrue(is_new_order_message(new_order_gmail_message))

        another_gmail_message = MagicMock(
            spec=Message,
            subject='Some test subject',
        )
        self.assertFalse(is_new_order_message(another_gmail_message))


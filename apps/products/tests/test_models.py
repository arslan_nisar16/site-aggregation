# import ipdb
from datetime import datetime, timedelta
from django.test import TestCase

from .. models import (
    Metrics,
    Product,
    Sale,
)

from .. factories import (
    MetricsFactory,
    SaleFactory,
    OwnProductFactory,
)


class ProductTestCase(TestCase):
    # there are 1 product and 6 metrics in the fixtures
    fixtures = ['product.json', 'metrics.json']

    def tearDown(self):
        Product.objects.all().delete()
        Metrics.objects.all().delete()
        Sale.objects.all().delete()

    def test_get_sales_per_hour(self):
        """Tests Product.get_sales_per_hour method"""
        product = Product.objects.get(title='test')
        sales_per_hour = product.get_sales_per_hour()
        self.assertEqual(
            sales_per_hour,
            [0.2083502135589689, 0.7916208552745789, 0.6250651109490573, 0.3750173619149035, 0.491332791699875]
        )

    def test_sales_rate(self):
        product = Product.objects.get(title='test')
        self.assertEqual(product.metrics.count(), 6)  # there are 6 metrics in fixtures/metrics.json
        self.assertEqual(round(product.sales_rate, 3), 0.222)

        # create one more Metric and check sales_rate again
        # the last Metric has timestamp '2016-01-19 00:06:59' and has 309 sales
        metric = MetricsFactory.create(
            product=product,
            sales=400,
        )
        metric.timestamp = datetime(2016, 1, 29, 20, 0)
        metric.save()
        product.update_sales_rate()
        # print metric.timestamp.strftime("%Y-%m-%d %H:%M:%S"), metric.sales
        self.assertEqual(metric.timestamp, datetime(2016, 1, 29, 20, 0))
        self.assertEqual(round(product.sales_rate, 3), 0.193)


class OwnProductTestCase(TestCase):

    def tearDown(self):
        Product.objects.all().delete()
        Metrics.objects.all().delete()
        Sale.objects.all().delete()

    def test_selling_speed(self):
        own_product = OwnProductFactory.create(selling_speed=0)
        self.assertEqual(round(own_product.selling_speed, 3), 0.0)

        first_transaction_datetime = datetime(2016, 1, 1)
        # create 7 Sale objects and check selling_speed again
        for day in range(7):
            SaleFactory.create(
                product=own_product,
                amount=30,
                transaction_datetime=first_transaction_datetime+timedelta(days=day)
            )
        self.assertEqual(own_product.sale_set.count(), 7)
        self.assertEqual(round(own_product.selling_speed, 3), 0.577)

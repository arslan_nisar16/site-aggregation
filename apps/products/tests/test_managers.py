#import ipdb
from datetime import (
    timedelta,
    datetime,
)
from django.test import TestCase

from .. models import Metrics, Product
from .. factories import ProductFactory, MetricsFactory


class StableProductManagerTestCase(TestCase):

    def tearDown(self):
        Product.objects.all().delete()
        Metrics.objects.all().delete()

    def test_stable_products_does_not_exist(self):
        """
        Tests that stable products does not exist in the database
        """
        ProductFactory.create_batch(5)
        # there are 5 products in the database
        self.assertEqual(Product.objects.count(), 5)
        # no one is stable
        self.assertEqual(Product.objects.with_stable_sales_rate().count(), 0)

    def test_stable_products_manager(self):
        """
        Tests stable products
        """
        product = ProductFactory.create()
        # create Metrics that is 35 days old
        MetricsFactory.create(
            timestamp=datetime.utcnow()-timedelta(days=35),
            product=product,
        )
        # still should NOT be 'stable' products in the database
        self.assertEqual(Product.objects.with_stable_sales_rate().count(), 0)

        # create three more Metricses
        MetricsFactory.create_batch(3, product=product)
        # update metrics' timestamp with datetime that is 35 days old
        product.metrics.all().update(
            timestamp=datetime.utcnow()-timedelta(days=35)
        )
        self.assertEqual(product.metrics.count(), 4)
        self.assertEqual(Product.objects.with_stable_sales_rate().count(), 1)

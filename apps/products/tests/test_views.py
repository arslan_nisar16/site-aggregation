import urllib
from django.core.urlresolvers import reverse
from django.test import TestCase, Client

from users.models import User

from .. models import Product

#import ipdb


class ProductListViewTestCase(TestCase):

    def setUp(self):
        self.client = Client()
        User.objects.create_superuser('test', 'test@email.com', 'password')
        self.client.login(username='test', password='password')

    def tearDown(self):
        Product.objects.all().delete()

    def test_search_by_url(self):
        """
        Tests ListProducts view search by URL
        """
        url = 'https://www.bukalapak.com' \
            '/p/komputer/hardware-1510/hard-disk-ram' \
            '/inemr-jual-hardisk-eksternal-wd-passport-ultra-2tb'
        product = Product.objects.create(url=url, category_id=1)

        resp = self.client.get(
            reverse('products:list') + '?keywords=%s' % urllib.quote(url, safe='')
        )
        self.assertEqual(200, resp.status_code)
        self.assertIn(product, resp.context['object_list'])

    def test_search_by_url_product_is_shown_first(self):
        """
        Test ListProducts view search by URL. Product with looked URL must be shown first
        """
        url = 'https://www.bukalapak.com' \
            '/p/komputer/hardware-1510/hard-disk-ram' \
            '/inemr-jual-hardisk-eksternal-wd-passport-ultra-2tb'

        # create a bunch of products (3 items) with similar URLs (ending differs)
        for url_ending in range(3):
            Product.objects.create(
                url='%s-%s' % (url, url_ending),
                category_id=1
            )
        product = Product.objects.create(url=url, category_id=1)

        resp = self.client.get(
            reverse('products:list') + '?keywords=%s' % urllib.quote(url, safe='')
        )
        self.assertEqual(200, resp.status_code)
        self.assertEqual(4, len(resp.context['object_list']))
        self.assertEqual(product, resp.context['object_list'][0])

# !/usr/bin/env python
# -*- coding: utf-8 -*-
import datetime
from django.utils.timezone import UTC
from django.utils.text import slugify

import factory
import factory.fuzzy

from .models import (
    Product,
    OwnProduct,
    Website,
    Category,
    Metrics,
    Sale,
)


class WebsiteFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Website
        django_get_or_create = (
            'base_url',
            'home_url',
            'title',
        )
    title = factory.Iterator(['Tokopedia', 'Bukalapak'])
    base_url = factory.LazyAttribute(lambda obj: 'www.%s.com' % obj.title.lower())
    home_url = factory.LazyAttribute(lambda obj: 'https://%s/' % obj.base_url)


class CategoryFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Category
        django_get_or_create = (
            'title',
            'url',
            # 'active',
            'website',
        )

    title = factory.Iterator([
        'Software',
        'Laptop',
        'Handphone',
        'Elektronik',
        'Komputer',
        'Handphone',
        'Kamera',
    ])

    website = factory.SubFactory(WebsiteFactory)
    url = factory.LazyAttribute(lambda obj: '%s%s' % (obj.website.home_url, obj.title.lower()))


class ProductFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Product
        django_get_or_create = ('title', 'category', 'url', 'imagePath')

    title = factory.Sequence(lambda n: 'Product #%s' % n)
    category = factory.SubFactory(CategoryFactory)
    url = factory.LazyAttribute(
        lambda obj: '%s/product/%s' % (obj.category.url, slugify(obj.title))
    )
    # URL of image that is available permanently
    imagePath = 'https://www.google.cz/images/branding/googlelogo/1x/googlelogo_color_272x92dp.png'


class OwnProductFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = OwnProduct
        django_get_or_create = ('title', 'url', 'website', 'selling_speed')

    title = factory.Sequence(lambda n: 'Own Product #%s' % n)
    selling_speed = factory.fuzzy.FuzzyDecimal(1, 2)
    website = factory.SubFactory(WebsiteFactory)
    url = factory.LazyAttribute(
        lambda obj: '%s/product/%s' % (obj.website.home_url, slugify(obj.title))
    )


class MetricsFactory(factory.django.DjangoModelFactory):

    class Meta:
        model = Metrics
        django_get_or_create = (
            'timestamp',
            'price',
            'sales',
            'product',
        )

    # timestamp = factory.Sequence(lambda n: datetime.datetime.now() + datetime.timedelta(days=n))
    timestamp = factory.fuzzy.FuzzyDateTime(
        datetime.datetime(2016, 1, 1, tzinfo=UTC()),
        datetime.datetime(2016, 12, 31, tzinfo=UTC()),
    )
    price = factory.fuzzy.FuzzyDecimal(100, 1000000, precision=2)
    sales = factory.fuzzy.FuzzyInteger(0, 1000)
    product = factory.SubFactory(ProductFactory)


class SaleFactory(factory.django.DjangoModelFactory):

    class Meta:
        model = Sale
        django_get_or_create = (
            'transaction_datetime',
            'amount',
            'product',
        )

    transaction_datetime = factory.fuzzy.FuzzyDateTime(
        datetime.datetime(2016, 1, 1, tzinfo=UTC()),
        datetime.datetime(2016, 12, 31, tzinfo=UTC()),
        force_day=3
    )
    amount = factory.fuzzy.FuzzyInteger(0, 100)
    product = factory.SubFactory(ProductFactory)

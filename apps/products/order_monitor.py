import httplib2
from dateutil import parser
import os
from apiclient import discovery
import oauth2client
from oauth2client import client
from oauth2client import tools
from datetime import date, timedelta, datetime

import logging
logger = logging.getLogger(__name__)
import coloredlogs
coloredlogs.install(level=logging.DEBUG)

class OrderMonitor:
  @classmethod
  def GetNewOrdersFromGmail(cls):
    credentials = cls.GetCredentials()
    http = credentials.authorize(httplib2.Http())
    gmailService = discovery.build('gmail', 'v1', http=http)

    messages = cls.GetNewMessages(gmailService)
    if not messages:
      raise ValueError('no new order exists')

    newOrders = []
    for message in messages:
      response = gmailService.users().messages().get(userId='me', format='metadata', id=message['id']).execute()
      import objectpath 
      responseJsonTree = objectpath.Tree(response)

      if not cls.IsOrderMessage(responseJsonTree):
        continue

      logger.debug("message['id']: %s" % message['id'])
      newOrder = {
        'orderMailId': message['id'],
        'orderDateTime': cls.GetOrderDateTime(responseJsonTree), 
        'orderProductUrl': cls.GetOrderProductUrl()
        }
      newOrders.append(newOrder)

    newOrders = sorted(newOrders, key=lambda k: k['orderDateTime']) 
    for newOrder in newOrders:
      logger.debug("---------GetNewMessages: newOrder['orderDateTime']: %s---------" % newOrder['orderDateTime'])
    return newOrders

  @classmethod
  def GetNewMessages(cls, gmailService):
    # Seems that userId can only be 'me' [](http://stackoverflow.com/questions/26135310/gmail-api-returns-403-error-code-and-delegation-denied-for-user-email)
    import time
    yesterday = (date.today() - timedelta(1)).strftime('%Y/%m/%d')
    query = 'subject:"Pesanan Baru" after:%s' % (yesterday)
    logger.info ("query: %s" % query)
    response = gmailService.users().messages().list(userId='me', q=query).execute()
    if 'messages' in response:
      return response['messages']
    return None

  @classmethod
  def IsOrderMessage(cls, responseJsonTree):
    for sender in responseJsonTree.execute('$..*[@.name is "From"].value'):
      # theoretically, only one sender exists in response
      if 'ec.south.asia@gmail.com' in sender:
        return True
    return False

  @classmethod
  def GetOrderDateTime(cls, responseJsonTree):
    for sentDate in responseJsonTree.execute('$..*[@.name is "Date"].value'):
      # sentDate: Mon, 5 Oct 2015 23:35:38 +0900
      # [](https://docs.python.org/2/library/datetime.html#datetime.datetime.strptime)
      # strptime cannot hander timezone [](http://stackoverflow.com/questions/3305413/python-strptime-and-timezones)
      #orderDateTime = datetime.strptime(sentDate[:-6], '%a, %d %b %Y %H:%M:%S')
      orderDateTime = parser.parse(sentDate)

      # theoretically, only one data exists in response
      return orderDateTime

  @classmethod
  def GetOrderProductUrl(cls):
    return 'http://dummy.url.com'

  @classmethod
  def GetCredentials(cls):
    """Gets valid user credentials from storage.

    If nothing has been stored, or if the stored credentials are invalid,
    the OAuth2 flow is completed to obtain the new credentials.

    Returns:
      Credentials, the obtained credential.
    """
    home_dir = os.path.expanduser('~')
    credential_dir = os.path.join(home_dir, '.credentials')
    logger.info ('credential_dir:%s' % credential_dir)
    if not os.path.exists(credential_dir):
      os.makedirs(credential_dir)
    credential_path = os.path.join(credential_dir, 'gmail-python-quickstart.json')

    store = oauth2client.file.Storage(credential_path)
    credentials = store.get()
    if not credentials or credentials.invalid:
      flow = client.flow_from_clientsecrets(cls.CLIENT_SECRET_FILE, cls.SCOPES)
      flow.user_agent = cls.APPLICATION_NAME
      if flags:
        credentials = tools.run_flow(flow, store, flags)
      else: # Needed only for compatability with Python 2.6
        credentials = tools.run(flow, store)
      logger.info('Storing credentials to ' + credential_path)
    return credentials


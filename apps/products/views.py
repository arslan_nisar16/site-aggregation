# -*- coding: utf-8 -*-
import os

from itertools import chain
from operator import and_
from datetime import datetime

from django.conf import settings
from django.shortcuts import render
from django.db.models import Q
from django.contrib import messages
from django.contrib.admin.views.decorators import staff_member_required
from django.core.urlresolvers import reverse
from django.http import HttpResponse, HttpResponseRedirect
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from django.utils.translation import ugettext as _
from django.views.generic import ListView
from django.views.generic.base import View
from django.views.generic.detail import DetailView
from django.views.generic.edit import (
    CreateView,
    UpdateView,
    DeleteView,
    FormView,
)

from django.core.validators import URLValidator
from django.core.exceptions import ValidationError

from braces.views import (
    CsrfExemptMixin,
    JSONResponseMixin,
    AjaxResponseMixin,
    StaffuserRequiredMixin,
)

from . forms import (
    ScrapyFormView,
    FeedbackForm,
    ProductSearchForm,
    ProductFilterForm,
)

from .models import Product, VisitorStatus, Metrics, OwnProduct
from .tasks import download_file_task
from .utils import read_file

import logging
import coloredlogs
logger = logging.getLogger(__name__)
coloredlogs.install(level=logging.DEBUG)


class DownloadProduct(CreateView):
    model = Product
    fields = ['title']
    template_name = 'common/create.html'
    success_url = '/products/'


def download_file(request):
    # filename = "/tmp/tmpdata_" + random_str()
    # job = download_file_task.delay(filename)
    # return render(request, "products/download.html", {"job_id": job.id, "filename": filename})
    return HttpResponse(
        'This page was <b>disabled</b>. You can find more details '
        '<a href="https://trello.com/c/Lfx4jFSr/29-t0836-segment-fault">here</a>'
    )


def download_status(request):
    if 'job' in request.GET:
        job_id = request.GET['job']
    else:
        return HttpResponse('No job id given.')

    if 'file' in request.GET:
        filename = request.GET['file']
    else:
        return HttpResponse('No filename given.')

    status = download_file_task.AsyncResult(job_id)
    if status.ready():
        # ##################################
        # ##         ATTENTION!!!        ###
        # ## Probably performance issue  ###
        # ##################################
        response = HttpResponse(read_file(filename), content_type='application/octet-stream')
        response['Content-Disposition'] = 'attachment; filename=download_data.csv'
        return response
    else:
        # return HttpResponseRedirect(reverse("products:download_status")+"?job="+job_id+"&file="+filename)
        return render(request, "products/download.html", {"job_id": job_id, "filename": filename})


def download_file2(request):
    import csv
    products = Product.objects
    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename=data.csv'

    writer = csv.writer(response)
    writer.writerow(
        ['URL', 'ID', 'internalId', 'Price', 'Stocks', 'Sales',
            'Page Views', 'Weight', 'Sales Rate', 'Similar Products', 'In stock'])

    for item in products.all()[0:10000]:
        writer.writerow([
            item.url,
            item.id,
            item.internalId,
            item.price,
            item.stock,
            item.sales,
            item.pageviews,
            item.weight,
            item.sales_rate,
            item.similar_products_num,
            item.in_stock
        ])

    return response


# TODO rename to ProductCreateView
class CreateProduct(StaffuserRequiredMixin, CreateView):
    model = Product
    fields = ['internalId', 'url', 'title', 'status']
    template_name = 'common/create.html'
    # BAD: hard-coded url. use 'reverse' instead
    success_url = '/products/'

    def form_valid(self, form):
        # Called when validation is passed
        # [](http://stackoverflow.com/questions/17826778/django-createview-is-not-saving-object)
        # [get field from form](http://stackoverflow.com/questions/4308527/django-model-form-object-has-no-attribute-cleaned-data)
        form.instance.url = form.cleaned_data['url']
        form.save()
        return super(CreateProduct, self).form_valid(form)


class MetricFormView(StaffuserRequiredMixin, CreateView):
    model = Metrics
    fields = ["product", "sales"]
    template_name = "common/create.html"
    success_url = '/products/metrics/'


class ProductListView(ListView):
    template_name = "products/list.html"
    model = Product
    filter_form_class = ProductFilterForm
    context_object_name = 'products'
    paginate_by = 25

    @method_decorator(csrf_exempt)
    def get(self, request, *args, **kwargs):
        self.filter_form = self.filter_form_class(request.GET)
        return super(ProductListView, self).get(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        form = FeedbackForm(data=request.POST)
        if form.is_valid():
            form.send_feedback()
            messages.success(request, _("Thank you for sending us your feedback."))
            return HttpResponseRedirect(reverse("products:list"))
        self.object_list = self.get_queryset()
        context = self.get_context_data()
        context["form"] = form
        return self.render_to_response(context)

    def get_sort_type(self):
        sort_type = self.request.GET.get("type", "asc")
        lookup = ["asc", "desc"]
        if sort_type not in lookup:
            sort_type = "asc"
        return sort_type

    def _get_sort_by_value(self):
        sort_type = self.get_sort_type()
        sort_by = self.request.GET.get("sort", "internalId")
        value = sort_by
        lookup = {
            "asc": {
                "stock": "stock",
                "internalId": "internalId",
                "weight": "weight",
                "price": "price",
                "pageviews": "pageviews",
                "sales": "sales",
                "sales_rate": "sales_rate",
                "similar_products": "similar_products_num",
                "similar_image_products": "similar_image_products_num",
            },
            "desc": {
                "stock": "-stock",
                "internalId": "-internalId",
                "weight": "-weight",
                "price": "-price",
                "pageviews": "-pageviews",
                "sales": "-sales",
                "sales_rate": "-sales_rate",
                "similar_products": "-similar_products_num",
                "similar_image_products": "-similar_image_products_num",
            }
        }
        if sort_type in lookup and sort_by in lookup[sort_type]:
            value = lookup[sort_type][sort_by]
        return value

    def get_website(self):
        try:
            return self.filter_form.cleaned_data.get('website')
        except:
            return

    def get_keywords(self):
        return self.request.GET.get("keywords", "").strip()

    def get_price(self):
        try:
            return self.filter_form.cleaned_data.get('price')
        except Exception:
            return

    def get_queryset(self):
        products = super(ProductListView, self).get_queryset()

        # default filter dict
        filter_dict = {'category__isnull': False}
        search_by_url = False  # "search by URL" flag
        search_by_keywords = False  # "search by keywords" flag

        if self.filter_form.is_valid():
            cd = self.filter_form.cleaned_data

            website = cd.get('website')
            if website:
                filter_dict.update({'category__website': website})

            category = cd.get('category')
            if category:
                filter_dict.update({'category': category})

            price = cd.get('price')
            if price:
                filter_dict.update({'price__gte': price})

        products = products.filter(**filter_dict)
        sort_by = self.request.GET.get("sort")
        keywords = self.get_keywords()

        if keywords:
            url_validate = URLValidator()
            try:
                # search by url
                url_validate(keywords)
                search_by_url = True
            except ValidationError:
                search_by_keywords = True

        if search_by_url:
            # search by url
            urls_parts = keywords.split('/')
            first_part = '/'.join(urls_parts[:-1])
            second_part = urls_parts[-1].split('-')

            query = '+'.join(["(CASE WHEN products_product.url LIKE '%%' || %s || '%%' THEN 1 ELSE 0 END)"] * len(second_part))
            # take products where rank >= 0.5
            products = products.filter(
                url__startswith=first_part
            ).extra(
                where=["({0})/{1}>=0.5".format(query, float(len(second_part))), ], params=second_part
            )

        elif search_by_keywords:
            # search by keywords
            products = products.filter(
                reduce(and_, (Q(title__icontains=kw) for kw in keywords.split()))
            )

        accepted_sorting = [
            "stock", "internalId", "weight", "price", "sales",
            "pageviews", "sales_rate", "similar_products",
            "similar_image_products"
        ]

        sort_value = self._get_sort_by_value()
        if sort_by in accepted_sorting:
            products = products.order_by(sort_value)
        # elif sort_by == "price":
        #     products = products.extra(select={
        #         "retail_price": "SELECT price FROM products_metrics \
        #         WHERE products_metrics.product_id = products_product.id \
        #         ORDER BY timestamp DESC LIMIT 1"
        #     })
        #     products = products.extra(order_by=[sort_value])
        # elif sort_by == "pageviews":
        #     products = products.extra(select={
        #         "views": "SELECT pageviews FROM products_metrics \
        #         WHERE products_metrics.product_id = products_product.id \
        #         ORDER BY timestamp DESC LIMIT 1"
        #     })
        #     products = products.extra(order_by=[sort_value])
        # elif sort_by == "sales":
        #     products = products.extra(select={
        #         "sale": "SELECT sales FROM products_metrics \
        #         WHERE products_metrics.product_id = products_product.id \
        #         ORDER BY timestamp DESC LIMIT 1"
        #     })
        #     products = products.extra(order_by=[sort_value])
        # elif sort_by == "sales_rate":
        #     products = products.extra(select={
        #         "sales_rate": "(SELECT sales FROM products_metrics \
        #         WHERE products_metrics.product_id = products_product.id \
        #         ORDER BY timestamp DESC LIMIT 1) - (SELECT sales FROM products_metrics \
        #         WHERE products_metrics.product_id = products_product.id \
        #         ORDER BY timestamp DESC LIMIT 1 OFFSET 1)",
        #         'sales_rate_is_null': '(SELECT sales FROM products_metrics \
        #         WHERE products_metrics.product_id = products_product.id \
        #         ORDER BY timestamp DESC LIMIT 1) IS NULL OR (SELECT sales FROM products_metrics \
        #         WHERE products_metrics.product_id = products_product.id \
        #         ORDER BY timestamp DESC LIMIT 1 OFFSET 1) IS NULL',
        #     })
        #     # make products with sales_rate=null last
        #     products = products.extra(order_by=['sales_rate_is_null', sort_value])
        else:
            products = products.order_by('internalId')

        if search_by_url:
            # if searching by URL then place product which has
            # exact URL into first position
            products = list(chain(
                products.filter(url=keywords),  # product with exact URL
                products.exclude(url=keywords)
            ))

        return products

    def get_context_data(self, **kwargs):
        context = super(ProductListView, self).get_context_data(**kwargs)
        # calculate rate by sales
        for product in context['object_list']:
            metrics = product.metrics.all().order_by('-timestamp')[:2]
            if len(metrics) < 2:
                product.rate = 'No data'
            else:
                product.rate = metrics[0].sales - metrics[1].sales
                if product.rate >= 0:
                    product.rate = '+%d' % product.rate

        context['sort_type'] = self.get_sort_type()
        context['form'] = FeedbackForm()
        context['keywords'] = self.get_keywords()
        context['filter_form'] = self.filter_form_class(self.request.GET)
        context['search_form'] = ProductSearchForm(self.request.GET)
        context['website'] = self.get_website()
        context['category'] = self.filter_form.cleaned_data.get('category')
        context['price'] = self.get_price()
        return context


class ProductUpdateView(
        CsrfExemptMixin,
        StaffuserRequiredMixin,
        JSONResponseMixin,
        AjaxResponseMixin,
        UpdateView):
    model = Product
    template_name = 'common/update.html'
    fields = CreateProduct.fields
    success_url = '/products/'

    def get_field_to_update(self):

        # name - name of field to be updated (column in db).
        # Taken from id or data-name attribute
        # http://vitalets.github.io/x-editable/docs.html
        return self.request.POST.get('name')

    def post_ajax(self, request, *args, **kwargs):
        self.object = self.get_object()
        field = self.get_field_to_update()
        value = self.request.POST.get('value')

        try:
            self.object.updated = datetime.utcnow()
            self.object.__setattr__(field, value)
            self.object.save(update_fields=[field, 'updated'])

        except Exception as ex:
            json_dict = {
                'success': False,
                'message': str(ex),
            }
        else:
            json_dict = {
                'success': True,
                'title': self.object.title,
                'updated': self.object.updated,
            }
        return self.render_json_response(json_dict)


class ProductDeleteView(StaffuserRequiredMixin, DeleteView):
    model = Product
    template_name = 'common/confirm_delete.html'
    success_url = '/products/'


def VisitProduct(request, myAffiliateId):
    ret = ParseAid(myAffiliateId)
    if not ret:
        return HttpResponse('incorrect affiliate link')

    (userId, productId) = ret

    visitorStatus = VisitorStatus(
        user_id=userId,
        product_id=productId,
        ip=GetIp(request))
    visitorStatus.save()

    product = Product.objects.get(id=productId)
    return HttpResponseRedirect(product.url)


def GetIp(request):
    from ipware.ip import get_ip
    ip = get_ip(request)
    if ip is not None:
        return ip
    else:
        return '0.0.0.0'


def ParseAid(myAffiliateId):
    from keyczar import keyczar
    location = '/tmp/kz'
    crypter = keyczar.Crypter.Read(location)
    s_decrypted = crypter.Decrypt(myAffiliateId)

    import re
    m = re.search('userId=(\d+),productId=(\d+)', s_decrypted)
    if not m:
        logger.debug("myAffiliateId: %s" % myAffiliateId)
        return None
    return (m.group(1), m.group(2))


class ListVisitorStatus(ListView):
    template_name = "products/visitor_status.html"
    context_object_name = 'visitorStatuses'

    def get_queryset(self):
        return VisitorStatus.objects.filter(user=self.request.user, product_id=self.kwargs['pk'])


class DeleteVisitorStatusView(StaffuserRequiredMixin, DeleteView):
    model = VisitorStatus

    def get_success_url(self):
        return self.kwargs['next']


class AdminCrawlSpiderView(View):

    @method_decorator(staff_member_required)
    def dispatch(self, request, *args, **kwargs):
        return super(AdminCrawlSpiderView, self).dispatch(request, *args, **kwargs)

    def get(self, request):
        scrapy_project = os.path.join(settings.BASE_DIR, 'scrapy_shopcrawler')
        spider = self.kwargs.get("spider")
        crawler_command = "%s %s" % ("scrapy crawl", spider)
        os.chdir(scrapy_project)
        os.system(crawler_command)
        return HttpResponseRedirect(reverse("products:list"))


class AdminScrapyFormView(FormView):

    template_name = "admin/products/scrapy.html"
    form_class = ScrapyFormView
    success_url = "/products/"

    @method_decorator(staff_member_required)
    def dispatch(self, request, *args, **kwargs):
        return super(AdminScrapyFormView, self).dispatch(request, *args, **kwargs)

    def form_valid(self, form):
        # This method is called when valid form data has been POSTed.
        # It should return an HttpResponse.
        spider = form.cleaned_data.get("spider")
        command = "scrapy crawl %s" % (spider, )
        scrapy_project = settings.SCRAPY_DIR
        os.chdir(settings.BASE_DIR)
        os.system("source venv/bin/activate")
        os.chdir(scrapy_project)
        os.system(command)
        return super(AdminScrapyFormView, self).form_valid(form)


class MetricsView(ListView):

    template_name = "products/metrics.html"
    # get ONLY products which have metrics
    queryset = Product.objects.filter(metrics__isnull=False).distinct('id')
    context_object_name = "products"
    paginate_by = 25


class ProductMetricsView(DetailView):
    template_name = "products/product_metrics.html"
    model = Product

    def get_metrics(self):
        return self.get_object().metrics.order_by("-timestamp")

    def get_context_data(self, **kwargs):
        context = super(ProductMetricsView, self).get_context_data(**kwargs)
        context['metrics'] = self.get_metrics()
        return context


class ProductSimilarView(DetailView):
    template_name = "products/product_similar.html"
    model = Product


class ProductSimilarImagesView(DetailView):
    model = Product
    template_name = "products/product_similar_images.html"


class ProductSellingSpeedView(ListView):
    model = OwnProduct
    queryset = OwnProduct.objects.filter(selling_speed__gt=0).order_by('-selling_speed')
    template_name = "product/product_selling_speed.html"


class UpdateMetrics(StaffuserRequiredMixin, UpdateView):
    model = Metrics
    template_name = 'common/update.html'
    # BAD: hard-coded url. use 'reverse' instead
    success_url = '/products/metrics/'
    fields = ['pageviews', 'price', 'sales', 'competitorNumber']
    # fields = CreateProvider.fields + ['serviceQualityScore', 'serviceQualityReview']


class DeleteMetrics(StaffuserRequiredMixin, DeleteView):
    model = Metrics
    template_name = 'common/confirm_delete.html'
    # BAD: hard-coded url. use 'reverse' instead
    success_url = '/products/metrics/'

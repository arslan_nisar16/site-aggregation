#-*- coding: utf-8 -*-
from django.conf.urls import patterns, url
from django.contrib.auth.decorators import login_required

import views


urlpatterns = [
    url(r'^metrics/create/$', views.MetricFormView.as_view(), name='metrics-create'),
    url(r'^metrics/update/(?P<pk>.*\d+)/$', views.UpdateMetrics.as_view(), name='update-metrics'),
    url(r'^metrics/delete/(?P<pk>.*\d+)/$', views.DeleteMetrics.as_view(), name='delete-metrics'),
    url(r'^(?P<pk>.*\d+)/metrics/$', views.ProductMetricsView.as_view(), name='product-metrics'),
    url(r'^metrics/$', views.MetricsView.as_view(), name='metrics'),

    url(r'^similar/(?P<pk>.*\d+)/$', views.ProductSimilarView.as_view(), name='product-similar'),
    url(r'^similar-images/(?P<pk>.*\d+)/$', views.ProductSimilarImagesView.as_view(), name='product-similar-images'),
    url(r'^selling/$', views.ProductSellingSpeedView.as_view(), name='selling_speed'),

    url(r'^create/$', views.CreateProduct.as_view(), name='create'),
    url(r'^download/$', views.download_file, name='download'),
    url(r'^download_status/$', views.download_status, name='download_status'),

    url(r'update/(?P<pk>.*\d+)/$',
        views.ProductUpdateView.as_view(),
        name='update'),

    url(r'delete/(?P<pk>.*\d+)/$',
        views.ProductDeleteView.as_view(),
        name='delete'),

    url(r'^VisitProduct/(?P<myAffiliateId>.+)/$', views.VisitProduct),

    url(r'^visitor-status/(?P<pk>.*\d+)/$', login_required(views.ListVisitorStatus.as_view()), name='visitor-status'),
    url(r'^VisitorStatus/Delete/(?P<pk>[0-9]+)/next=(?P<next>.+)/$',
        views.DeleteVisitorStatusView.as_view(),
        name='DeleteVisitorStatus'),

    url(r'^$', views.ProductListView.as_view(), name='list'),
]

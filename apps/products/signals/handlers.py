# -*- coding: utf-8 -*-
from django.db.models.signals import post_save
from django.dispatch import receiver

from .. models import Metrics, Sale

import logging
import coloredlogs
logger = logging.getLogger(__name__)
coloredlogs.install(level=logging.DEBUG)


@receiver(post_save, sender=Metrics)
def update_product_metrics(sender, instance, created, **kwargs):
    """Updates product with latest Metric and calculates new sales_rate
    """
    logger.info("Updates product with latest Metric and calculates new sales_rate")
    if not created:
        return
    product = instance.product
    product.update_with_metric(instance)
    product.save()


@receiver(post_save, sender=Sale)
def update_product_selling_speed(sender, instance, created, **kwargs):
    """Updates product's selling speed with just created Sale object"""
    logger.info("Updates product's selling speed")
    if not created:
        return
    product = instance.product
    product.update_selling_speed()

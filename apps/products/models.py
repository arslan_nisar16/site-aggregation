# -*- coding: utf-8 -*-
# import ipdb
import numpy
from decimal import Decimal

from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.conf import settings
# from relish.helpers import upload_to
from users.models import User

from . managers import ProductManager

import logging
import coloredlogs
logger = logging.getLogger(__name__)
coloredlogs.install(level=logging.DEBUG)

DEFAULT_IMAGEPATH = 'dummy image path'


class Shop(models.Model):
    title = models.CharField(max_length=255)
    url = models.URLField(unique=True, max_length=255)
    feedbackNumber = models.PositiveIntegerField(default=0)
    score = models.FloatField(default=0.0)
    lastVisited = models.DateTimeField(auto_now_add=True)

    class Meta:
        # These models are also used by scrapy.
        # Django automatically adds app label as the table prefix.
        # We need to tell this prefix to scrapy.
        db_table = "products_shop"


class ProductStatus(models.Model):
    status = models.CharField(
        max_length=255, default='dummy product status', null=False)

    def __unicode__(self):
        return self.status

    class Meta:
        verbose_name = _("Product Status")
        verbose_name_plural = _("Product Statuses")
        db_table = "products_productstatus"


# insert default statuses
# when the first time manage.py migrate is called, products_productstatus
# table is not created yet. get_or_create() will fail
try:
    ProductStatus.objects.get_or_create(status='1_not_yet_processed')[0].save()
    ProductStatus.objects.get_or_create(
        status='2_ordered_not_arrived')[0].save()
    ProductStatus.objects.get_or_create(status='3_ordered_arrived')[0].save()
    ProductStatus.objects.get_or_create(status='4_to_be_ordered')[0].save()
    ProductStatus.objects.get_or_create(
        status='5_not_to_order_any_more (low profit)')[0].save()
    ProductStatus.objects.get_or_create(
        status='6_not_to_order_any_more (no provider)')[0].save()
    ProductStatus.objects.get_or_create(
        status='7_not_to_order_any_more (high purchase price)')[0].save()
except:
    pass


class Website(models.Model):
    base_url = models.CharField(max_length=255, unique=True)
    home_url = models.URLField(unique=True, max_length=255)
    title = models.CharField(max_length=255, unique=True)

    def __unicode__(self):
        return u'{s.title}'.format(s=self)


class Category(models.Model):
    url = models.URLField(unique=True, max_length=1000)
    title = models.CharField(null=False, max_length=1000)
    active = models.BooleanField(default=True)
    website = models.ForeignKey(Website, null=True, related_name='categories')

    def __unicode__(self):
        return u'{s.title}'.format(s=self)


class BaseProduct(models.Model):
    """Base product model"""
    url = models.URLField(unique=True, max_length=1000, db_index=True)
    title = models.CharField(null=False, max_length=1000, db_index=True)

    class Meta:
        abstract = True

    def update_field(self, field, value):
        """ Updates single product's field with new value """
        self.__setattr__(field, value)
        return self.save(update_fields=[field, ])


class Product(BaseProduct):
    internalId = models.PositiveIntegerField(null=True, db_index=True)
    description = models.TextField(default="dummy description")
    imagePath = models.CharField(max_length=255, default=DEFAULT_IMAGEPATH)
    similarity = models.FloatField(default=0.0)  # the simularity of this products on buka and lazada
    category = models.ForeignKey(
        Category,
        null=True,
        related_name='products',
        on_delete=models.PROTECT,
    )
    # Formula for this 'weight' field will be 'pageviews' divided by 'price'
    weight = models.DecimalField(
        max_digits=20, decimal_places=3, default=Decimal("0.000"))

    status = models.ForeignKey(ProductStatus, null=True)
    sellingUrls = models.URLField(null=True, max_length=255)
    stock = models.PositiveIntegerField(default=1, null=True)

    # One-To-Many relationship (shop has many products)
    # sometime, shop is not important. So, set null = True
    shop = models.ForeignKey(Shop, related_name='products', null=True)
    # searchKeys = models.ForeignKey(SearchKeys, related_name = 'products' )
    similar_products_num = models.PositiveIntegerField(
        default=0, db_index=True)
    manual_similar_products_num = models.PositiveIntegerField(
        default=0, db_index=True)
    updated = models.DateTimeField(
        null=True,
        help_text="Date and Time when manual_similar_products_num was updated last time")
    similar_products = models.ManyToManyField('self')
    similar_image_products = models.ManyToManyField('self')
    similar_image_products_num = models.PositiveIntegerField(default=0)

    price = models.DecimalField(
        max_digits=20,
        decimal_places=0,
        default=Decimal("0"),
        db_index=True
    )
    sales = models.PositiveIntegerField(default=0, null=True, db_index=True)
    pageviews = models.PositiveIntegerField(
        default=0, null=True, db_index=True)
    sales_rate = models.FloatField(default=0, db_index=True)
    # "Sales rate" is for other sellers products and "selling speed" is for my own products.
    in_stock = models.BooleanField(default=True)
    last_google_image_scraped_time = models.DateTimeField(null=True)

    objects = ProductManager()  # custom product manager

    class Meta:
        verbose_name = _('Product')
        verbose_name_plural = _('Products')
        db_table = "products_product"

    def __unicode__(self):
        return "id:%s, internalId:%s, %s" % (self.id, self.internalId, self.title)

    def get_image_url(self):
        if self.image:
            return self.image.url
        else:
            return settings.DEFAULT_IMAGE

    def get_real_image_or_none(self):
        if not self.imagePath or self.imagePath == DEFAULT_IMAGEPATH:
            return
        return self.imagePath

    def update_similar_product_num(self):
        self.similar_products_num = self.similar_products.count()

    def update_similar_image_products_num(self):
        self.similar_image_products_num = self.similar_image_products.count()

    def update_with_metric(self, metric):
        self.price = metric.price
        self.sales = metric.sales
        self.pageviews = metric.pageviews
        self.update_sales_rate()

    def get_sales_per_hour(self):
        """Returns list of 'sales per hour' values"""
        prev_metric = None
        sales_values = []
        # sales_per_hour = time_delta_sec = None  # for debug purpose
        for metric in self.metrics.order_by('timestamp'):
            if prev_metric:
                try:
                    time_delta = metric.timestamp - prev_metric.timestamp
                    time_delta_sec = time_delta.total_seconds()
                    sales_per_hour = 1.0 * (metric.sales - prev_metric.sales) \
                        / (time_delta_sec / 3600.0)
                    if sales_per_hour < 0:
                        sales_per_hour = 0
                except ZeroDivisionError:
                    # can happen if there are less than an hour between crawls
                    # skip it
                    continue
                sales_values.append(sales_per_hour)
            prev_metric = metric
            # print metric.timestamp.strftime("%Y-%m-%d %H:%M:%S"), metric.sales, time_delta_sec, sales_per_hour
        # print sales_values
        return sales_values

    def update_sales_rate(self):
        """Calculate product sales rate based on all metrics
        x(i) = moving average sales until the i-th crawl
        k(i) = sales rate until the i-th crawl
         t*((x(i)-x(i-1))/x(i)) + (1-t) * k(i-1), where t=0.5
        """
        current_sales_rate = 0
        sales_values = self.get_sales_per_hour()
        if sales_values:
            # moving average
            mov_average_num = 4
            window = numpy.ones(int(mov_average_num)) / float(mov_average_num)
            average_sales_values = numpy.convolve(sales_values, window)
            t = 0.5
            for sr in average_sales_values:
                current_sales_rate = t * sr + (1 - t) * current_sales_rate
        # save only 'sales_rate' value, but not whole product
        self.update_field('sales_rate', current_sales_rate)


class OwnProduct(BaseProduct):
    """
    https://trello.com/c/UYTNRhb2/30-t2222-measure-sales-speed-by-parsing-mails
    I am selling products on bukalapak and tokopidia.
    I need to trace the selling speed of my products.
    buka and toko send me a notification mail once a new order is made.
    """
    website = models.ForeignKey(Website)
    # You can calculate selling speed using the algorithm for "sales rate".
    selling_speed = models.FloatField(default=0, db_index=True)

    class Meta:
        unique_together = ('url', 'website')

    def __unicode__(self):
        return u'%s' % (self.title or self.url)

    def has_valid_url(self):
        return self.url.startswith('http') and self.url.count(self.website.base_url)

    def update_selling_speed(self):
        """The crawler has been calculating "sales rate", right?
        "Sales rate" is for other sellers products and "selling speed" is for my own products.
        You can calculate selling speed using the algorithm for "sales rate".
        """
        prev_sale = None
        sales_values = []
        for sale in self.sale_set.all():
            if prev_sale:
                try:
                    time_delta = sale.transaction_datetime - prev_sale.transaction_datetime
                    time_delta_sec = time_delta.total_seconds()
                    logger.debug('time_delta_sec: %s' % time_delta_sec)
                    sales_per_hour = (1.0 * sale.amount) / (time_delta_sec / 3600.0)
                    logger.debug('sales_per_hour: %s' % sales_per_hour)
                    sales_values.append(sales_per_hour)
                except ZeroDivisionError as ex:
                    logger.debug(ex)
                    continue
            prev_sale = sale

        if sales_values:
            # moving average
            mov_average_num = 4
            window = numpy.ones(int(mov_average_num)) / float(mov_average_num)
            average_sales_values = numpy.convolve(sales_values, window)
            t = 0.5
            current_selling_speed = 0
            for sr in average_sales_values:
                current_selling_speed = t * sr + (1 - t) * current_selling_speed
            # save only 'selling_speed' value, but not whole product
            self.update_field('selling_speed', current_selling_speed)


class SearchKey(models.Model):
    searchKey = models.CharField(max_length=255)
    subSearchKey = models.CharField(max_length=255)
    product = models.ForeignKey(Product)

    class Meta:
        db_table = "products_searchkey"


class Metrics(models.Model):

    '''
    We trace the data of each product at different times.
    So, each product has multiple metrics
    '''
    timestamp = models.DateTimeField(auto_now_add=True, db_index=True)
    pageviews = models.PositiveIntegerField(
        default=0, null=True, db_index=True)
    likes = models.PositiveIntegerField(default=0, null=True)
    price = models.DecimalField(
        max_digits=20, decimal_places=0,
        default=Decimal("0"), db_index=True
    )
    sales = models.PositiveIntegerField(default=0, null=True, db_index=True)
    commentNumber = models.PositiveIntegerField(default=0, null=True)
    score = models.FloatField(default=0.0, null=True)
    competitorNumber = models.PositiveIntegerField(default=0, null=True)
    # stock = models.PositiveIntegerField(default=0)
    # One-To-Many relationship (product has many metrics)
    product = models.ForeignKey(Product, related_name='metrics')

    class Meta:
        db_table = "products_metrics"

    @property
    def weight(self):
        if self.competitorNumber > 0:
            return self.sales / self.competitorNumber
        return 0


# class Order(models.Model):
#     """ card T2222 related model
#     was commented due to workflow.mdj update
#     __doc__ string for Order model
#     """
#     amount = models.PositiveIntegerField(default=0, null=True)


class Sale(models.Model):
    product = models.ForeignKey(OwnProduct)
    # order = models.ForeignKey(Order)
    amount = models.PositiveIntegerField(default=0)
    transaction_datetime = models.DateTimeField(db_index=True)
    created = models.DateTimeField(auto_now_add=True, editable=False)

    class Meta:
        unique_together = ('product', 'transaction_datetime')
        ordering = ('transaction_datetime', )

    # @property
    # def amount(self):
    #     return self.order.amount


class Keywords(models.Model):
    keyword = models.CharField(unique=True, max_length=255)
    seedKeyword = models.CharField(max_length=255)
    seedExpander = models.CharField(max_length=255)

    searchVolume = models.PositiveIntegerField()
    averageCpc = models.IntegerField()
    competition = models.FloatField()
    productNumber = models.PositiveIntegerField()

    class Meta:
        db_table = "products_keywords"

    # TODO figure out what the fuck is it?
    def Init(
        self, _seedKeyword='', _seedExpander='', _keyword='', _searchVolume=-1,
            _averageCpc=-1, _competition=-1, _productNumber=-1):
        self.seedKeyword = _seedKeyword
        self.seedExpander = _seedExpander
        self.keyword = _keyword
        self.searchVolume = _searchVolume
        self.averageCpc = _averageCpc
        self.competition = _competition
        self.productNumber = _productNumber


class VisitorStatus(models.Model):
    user = models.ForeignKey(User)
    product = models.ForeignKey(Product)

    time = models.DateTimeField(auto_now_add=True)
    ip = models.GenericIPAddressField()

    isOrder = models.BooleanField(default=False)
    orderMailId = models.CharField(max_length=32, null=True)
    orderMailDate = models.DateTimeField(null=True)

#-*- coding: utf-8 -*-

from django.apps import AppConfig


class ProductConfig(AppConfig):
    name = 'products'
    verbose_name = "Products Application"


    def ready(self):
        from . signals import handlers

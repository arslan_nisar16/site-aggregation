from django import template
register = template.Library()


@register.filter
def switch_type(sort_type):
    """
    This switches the given 'sort_type' to its corresponding other part.
    Example:
        - if 'sort_type' is equal to 'asc' then return 'desc'
    """
    value = "desc"
    lookup = {
        "asc": "desc",
        "desc": "asc"
    }
    if sort_type in lookup:
        value = lookup[sort_type]
    return value

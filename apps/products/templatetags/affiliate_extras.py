import os

from django import template
from django.conf import settings
register = template.Library()

from keyczar import keyczar
import logging
logger = logging.getLogger(__name__)
import coloredlogs
coloredlogs.install(level=logging.DEBUG)

@register.filter
def GetAid(userId, productId):
  s = 'userId=%s,productId=%s' % (userId, productId)
  location = os.path.join(os.path.join(settings.BASE_DIR, "config"), "keyczarkey")
  crypter = keyczar.Crypter.Read(location)
  s_encrypted = crypter.Encrypt(s)

  logger.debug("userId: %s" % userId)
  logger.debug("productId: %s" % productId)
  logger.debug("s_encrypted: %s" % s_encrypted)
  return s_encrypted

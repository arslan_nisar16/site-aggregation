from django.core.management import call_command

import logging
logger = logging.getLogger(__name__)

from goodobtain.celery import app
from products.utils import gen_data_file


@app.task(bind=True)
def backup_database(self):
    logger.info("[DATABASE-BACKUP] Executing Backup...")
    call_command("dbbackup")


@app.task(name="download_file_task")
def download_file_task(filename):
    logger.info("download file task")
    logger.info("disabled: https://trello.com/c/Lfx4jFSr/29-t0836-segment-fault")

    #gen_data_file(filename)

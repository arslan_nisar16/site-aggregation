# -*- coding: utf-8 -*-
import warnings

from django.http import HttpResponse
from django.utils.crypto import get_random_string

from .. models import Product


def gen_data_file(filename):
    # import csv
    products = Product.objects

    csvfile = open(filename, 'w')
    # csvfile = file(filename,'wb')
    # writer = csv.writer(csvfile)

    # csv file head title
    # writer.writerow(['URL', 'ID', 'internalId', 'Price', 'Stocks',
    #    'Sales', 'Page Views', 'Weight', 'Sales Rate', 'Similar Products', 'In stock'])
    csvfile.write('URL,')
    csvfile.write('ID,')
    csvfile.write('internalId')
    csvfile.write('Price')
    csvfile.write('Stocks')
    csvfile.write('Sales,')
    csvfile.write('Page Views,')
    csvfile.write('Weight,')
    csvfile.write('Sales Rate,')
    csvfile.write('Similar Products,')
    csvfile.write('In stock\n')

    # csv file content, get all data from database
    for item in products.all():
        # writer.writerow([item.url, item.id, item.internalId, item.price, item.stock,
        # item.sales, item.pageviews, item.weight, item.sales_rate,
        # item.similar_products_num, item.in_stock])

        row = str(item.url) + ',' + str(item.id) + ',' + str(
            item.internalId) + ',' + str(item.price) + ',' + str(item.stock) + ','
        row += str(item.sales) + ',' + str(item.pageviews) + \
            ',' + str(item.weight) + ',' + str(item.sales_rate) + ','
        row += str(item.similar_products_num) + ',' + str(item.in_stock) + '\n'

        csvfile.write(row)

    csvfile.close()


def read_file(filename, buf_size=8192):
    with open(filename, "rb") as f:
        while True:
            content = f.read(buf_size)
            if content:
                yield content
            else:
                break


def random_str(randomlength=8):
    # TODO delete this function

    # import random
    # str = ''
    # chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789'
    # length = len(chars) - 1
    # random = random.Random()
    # for i in range(randomlength):
    #    str += chars[random.randint(0, length)]
    # return str

    warnings.warn(
        "Don't reinvent the wheel. "
        "Use the existing functions instead.\n"
        "e.g.\n"
        "django.utils.crypto.get_random_string "
        "should be used in this case."
    )

    return get_random_string()


def download_file_func(request):
    filename = "/tmp/tmpdata_" + random_str()
    gen_data_file(filename)

    response = HttpResponse(
        read_file(filename), content_type='application/octet-stream')
    # response = ['Content-Length'] = os.path.getsize(filename)
    response['Content-Disposition'] = 'attachment; filename=download_data.csv'

    return response

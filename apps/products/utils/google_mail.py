# -*- coding: utf-8 -*-
import re
import requests
from scrapy.selector import Selector

from dateutil import parser

from gmail import Gmail

WEBSITE_BUKALAPAK = 'bukalapak.com'
WEBSITE_TOKOPEDIA = 'tokopedia.com'


def get_unread_messages(username, password, from_sender='ec.south.asia@gmail.com', **kwargs):
    """get unread messages sent from ec.south.asia@gmail.com"""
    g = Gmail()
    g.login(username, password)
    unread_messages = g.inbox().mail(unread=True, sender=from_sender, **kwargs)
    for msg in unread_messages:
        msg.fetch()
        yield msg
    return


def get_product_url(mandrillapp_url):
    """Returns real product's URL from the 'mandrillapp track click' URL

       :param mandrillapp_url: 'str' or 'unicode'

       e.g. http://mandrillapp.com/track/click/30624249/www.bukalapak.com?p=eyJzIjoiekVBUzBFZF
       redirects to
       https://www.bukalapak.com/products/jlcax-jual-oem-samsung-official-elegant-cover-galaxy-tab-s-8-4-t700-t705
    """
    r = requests.head(mandrillapp_url)
    return r.headers.get('Location') or mandrillapp_url


def parse_bukalapak_order_message(gmail_message):
    """Returns new order dict from email received from bukalapak.com

       :param gmail_message: 'gmail.message.Message' instance with pre-fetched body,
       html and other fields
    """
    selector = Selector(text=gmail_message.html)

    # transaction_datetime = selector.xpath(
    #     '/html/body/div/div/div/center/table/tbody/tr/td/table/tbody/tr[2]/td/table/tbody/tr[2]'
    #     '/td[2]/table[1]/tbody/tr/td/div[2]/table/tbody/tr[2]/td[2]/strong/text()'
    # ).extract_unquoted()[0]

    # use http://labix.org/python-dateutil to get datetime from e-mail header
    # instead of parsing HTML
    transaction_datetime = parser.parse(gmail_message.headers.get('Date'))

    amount = selector.xpath(
        '/html/body/div/div/div/center/table/tbody/tr/td/table/tbody/tr[2]/td/table/tbody/tr[2]'
        '/td[2]/table[1]/tbody/tr/td/div[2]/table/tbody/tr[9]/td[2]/text()'
    ).extract()[0]
    # convert to int
    # TODO catch exception
    amount = int(amount)

    # get mandrillapp product link
    product_url = selector.xpath(
        "/html/body/div/div/div/center/table/tbody/tr/td/table/tbody/tr[2]/td/table/tbody/tr[2]"
        "/td[2]/table[1]/tbody/tr/td/div[2]/table/tbody/tr[9]/td[1]"
        "/a[contains(@href, 'http://mandrillapp.com/track/click/') and contains(@href, 'bukalapak')]"
        "/@href"
    ).extract()[0]
    # get real product_url that points to product on the bukalapak.com
    product_url = get_product_url(product_url)

    return dict(
        amount=amount,
        product_url=product_url,
        transaction_datetime=transaction_datetime,
        website=WEBSITE_BUKALAPAK,
    )


def parse_tokopedia_order_message(gmail_message):
    """Returns new order dict from email received from tokopedia.com

       :param gmail_message: 'gmail.message.Message' instance with pre-fetched body,
       html and other fields
    """
    selector = Selector(text=gmail_message.html)
    transaction_datetime = parser.parse(gmail_message.headers.get('Date'))
    amount = selector.xpath(
        "//*[text()[contains(.,'Jumlah: ')]]/text()"
    ).extract()[0]

    search_result = re.search('Jumlah: (\d+) Buah', amount)
    if search_result:
        amount = search_result.groups()[0]

    # convert to int
    # TODO catch exception
    amount = int(amount)

    # get mandrillapp product link
    product_title = selector.xpath(
        '/html/body/div/div/div/table[2]/tbody/tr/td[2]/div/table/tbody/tr/td/div[2]/div'
        '/ol/li/b/text()'
    ).extract()[0]

    return dict(
        transaction_datetime=transaction_datetime,
        amount=amount,
        product_title=product_title,
        website=WEBSITE_TOKOPEDIA,
    )


def get_message_website(gmail_message):
    """Returns website title from which the message was sent
    e.g. 'bukalapak' or 'tokopedia'
    """
    message_body = gmail_message.body.lower()
    # if filter(lambda x: 'bukalapak' in x.lower(),  gmail_message.headers.values()):
    if 'bukalapak' in message_body:
        return 'bukalapak'
    # elif filter(lambda x: 'tokopedia' in x.lower(),  gmail_message.headers.values()):
    elif 'tokopedia' in message_body:
        return 'tokopedia'
    return None


def parse_order_message(gmail_message):
    """Returns order dict from the email message

       :param gmail_message: 'gmail.message.Message' instance
       with pre-fetched 'body', 'html' and other fields
    """
    try:
        message_parser = 'parse_%s_order_message' % get_message_website(gmail_message)
        return eval(message_parser)(gmail_message)
    except NameError:
        raise Exception(
            'Unknown message sender. '
            'Can not determine who did send it: '
            'bukalapak or tokopedia?'
        )


def is_new_order_message(gmail_message):
    """Returns True if gmail_message contains 'Pesanan baru' in subject
    Indonesian 'Pesanan baru' is translated to English as 'New orders'
    """
    return 'pesanan baru' in gmail_message.subject.lower()

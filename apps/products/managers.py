#-*- coding: utf-8 -*-
from itertools import chain
from datetime import timedelta

from django.db import models
from django.db.models import Count, Min
from django.utils import timezone


def datetime_threshold(days=30):
    return timezone.now() - timedelta(days=days)


class ProductManager(models.Manager):

    annotate_stable_product = {
        'num_metrics': Count('metrics'),  # number of metrics for a product
        'first_metric_timestamp': Min('metrics__timestamp'),  # first product's metric timestamp
    }

    stable_product_filter = {
        'num_metrics__gt': 3,  # crawled more than 3 times
        'first_metric_timestamp__lt': datetime_threshold(days=30)
    }

    unstable_product_filter = {
        'num_metrics__lte': 3,  # crawled 3 times or less
        'first_metric_timestamp__gt': datetime_threshold(days=30)  # is younger than 30 days
    }

    def get_queryset(self):
        return super(ProductManager, self).get_queryset()

    def with_stable_sales_rate(self):
        """
        Product's sales rate became relatively stable after:
            - after the product has been crawled for more than 3 times
            - and more than 30 days
        """
        return self.get_queryset().annotate(
            **self.annotate_stable_product
        ).filter(
            **self.stable_product_filter
        )

    def with_unstable_sales_rate(self):
        """
        Returns queryset of products that were:
            - crawled for ONLY 3 times or less
            - crawled for the first time during latest 30 days
        """
        return self.get_queryset().annotate(
            **self.annotate_stable_product
        ).filter(
            **self.unstable_product_filter
        )

    def to_crawl(self):
        """
        Returns queryset of products that should be crawled and updated

        Related card: https://trello.com/c/iYEae0A3/26-t0531-accelerate-the-crawling
        """
        price_filter_dict = {
            'price__gte': 80000,
            'price__lte': 500000,
        }
        sales_rate_filter_dict = {
            'sales_rate__gt': 0.6,
        }

        stable_products_queryset = self.with_stable_sales_rate().filter(
            **price_filter_dict
        ).filter(**sales_rate_filter_dict)

        unstable_products_queryset = self.with_unstable_sales_rate()

        result_list = sorted(
            chain(stable_products_queryset, unstable_products_queryset),
            key=lambda instance: instance.sales_rate,
            reverse=True,
        )
        return result_list

# -*- coding: utf-8 -*-
from django import forms
from django.conf import settings
from django.core.mail import send_mail

from .models import (
    Category,
    Website,
)

#import ipdb


class ScrapyFormView(forms.Form):
    """
    Form that accepts scrapy commands
    """
    SPIDERS = [
        ("lazadaAllShopSpider", "Lazada All Shop"),
        ("allShopSpider", "Bukalapak All Shop"),
        ("CategoryBukalapakSpider", "Bukalapak Categories"),
        ("ExistingBukalapakSpider", "Existing Bukalapak Products from Database"),
        ("tokoSpider", "Tokopedia active Categories"),
        ("existingTokoSpider", "Existing Tokopedia Category Products from Database")
    ]
    spider = forms.ChoiceField(choices=SPIDERS)


class FeedbackForm(forms.Form):
    """
    Form for sending feedback to admin@goodobtain.com
    """
    feedback = forms.CharField(widget=forms.Textarea)

    def __init__(self, *args, **kwargs):
        super(FeedbackForm, self).__init__(*args, **kwargs)
        self.fields['feedback'].widget.attrs['class'] = 'form-control'

    def send_feedback(self):
        data = self.cleaned_data
        subject = "Crawler Site Feedback"
        message = data.get("feedback")
        send_mail(
            subject,
            message,
            settings.DEFAULT_FROM_EMAIL,
            [settings.ADMIN_EMAIL],
            fail_silently=False
        )


class ProductSearchForm(forms.Form):
    keywords = forms.CharField(required=False)

    def __init__(self, *args, **kwargs):
        super(ProductSearchForm, self).__init__(*args, **kwargs)
        self.fields['keywords'].widget.attrs['class'] = 'form-control'


class ProductFilterForm(forms.Form):
    website = forms.ModelChoiceField(
        empty_label='All websites',
        queryset=Website.objects.all(),
        required=False,
    )
    category = forms.ModelChoiceField(
        empty_label='All categories',
        queryset=Category.objects.all(),
        required=False,
    )
    price = forms.IntegerField(
        label='Price higher than',
        required=False,
    )

    def __init__(self, *args, **kwargs):
        super(ProductFilterForm, self).__init__(*args, **kwargs)
        self.fields['category'].choices = self._get_category_choices()

    def _get_category_choices(self):
        choices = [(None, 'All categories')]
        for website in Website.objects.all().order_by('title'):
            choices.append(
                (
                    website.title,
                    [(category.id, '%s - %s' % (category.website, category.title)) for category in website.categories.all()]
                )
            )
        return choices

    def clean_category(self):
        website = self.cleaned_data.get('website')
        category = self.cleaned_data.get('category')
        if website and category:
            if category.website != website:
                raise forms.ValidationError("Category %s does not belong to %s" % (category, website))
        return category

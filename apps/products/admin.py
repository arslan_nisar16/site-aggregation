# -*- coding: utf-8 -*-
from django.contrib import admin

from . models import Product, ProductStatus, OwnProduct, Category, Website, Sale


class ProductAdmin(admin.ModelAdmin):
    list_display = [
        'title',
        'category',
        'price',
        'sales',
        'sales_rate',
    ]
    list_filter = [
        'category__website',
        'category__active',
        'category',
    ]
    raw_id_fields = ['shop', 'similar_products', 'similar_image_products']
    search_fields = ["title", "url"]
    exclude = ("metrics", )


class OwnProductAdmin(admin.ModelAdmin):
    list_display = [
        'title',
        'url',
        'selling_speed',
    ]

    list_filter = [
        'website',
    ]
    search_fields = ["title", "url"]


class WebsiteAdmin(admin.ModelAdmin):
    list_display = ['base_url', 'title']


class CategoryAdmin(admin.ModelAdmin):
    list_display = ["title", "url", "active"]
    list_filter = ['website', 'active']
    search_fields = ["title", "url"]


class ProductStatusAdmin(admin.ModelAdmin):
    model = ProductStatus


class SaleAdmin(admin.ModelAdmin):
    list_display = [
        'product',
        'amount',
        'transaction_datetime',
    ]

    # list_filter = [
    #     'category__website',
    #     'category__active',
    #     'category',
    # ]
    raw_id_fields = ['product', ]


admin.site.register(Category, CategoryAdmin)
admin.site.register(Product, ProductAdmin)
admin.site.register(OwnProduct, OwnProductAdmin)
admin.site.register(ProductStatus, ProductStatusAdmin)
admin.site.register(Website, WebsiteAdmin)
admin.site.register(Sale, SaleAdmin)

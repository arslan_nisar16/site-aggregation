# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('products', '0021_auto_20151121_0930'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='productstatus',
            options={'verbose_name': 'Product Status', 'verbose_name_plural': 'Product Statuses'},
        ),
        migrations.AlterField(
            model_name='product',
            name='title',
            field=models.CharField(max_length=1000),
        ),
        migrations.AlterField(
            model_name='product',
            name='url',
            field=models.URLField(unique=True, max_length=1000),
        ),
    ]

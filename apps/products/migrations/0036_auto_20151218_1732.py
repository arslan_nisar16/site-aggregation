# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('products', '0035_product_in_stock'),
    ]

    operations = [
        migrations.AlterField(
            model_name='shop',
            name='feedbackNumber',
            field=models.PositiveIntegerField(default=0),
        ),
    ]

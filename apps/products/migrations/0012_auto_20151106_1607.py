# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('products', '0011_remove_product_test'),
    ]

    operations = [
        migrations.AlterField(
            model_name='product',
            name='retailPrice',
            field=models.PositiveIntegerField(default=999, verbose_name='Retail price'),
        ),
    ]

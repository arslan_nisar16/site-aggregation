# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('products', '0022_auto_20151207_0940'),
    ]

    operations = [
        migrations.AddField(
            model_name='product',
            name='similar_products_num',
            field=models.PositiveIntegerField(default=0),
        ),
    ]

# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('products', '0036_auto_20151218_1732'),
    ]

    operations = [
        migrations.AddField(
            model_name='product',
            name='last_google_image_scraped_time',
            field=models.DateTimeField(null=True),
        ),
    ]

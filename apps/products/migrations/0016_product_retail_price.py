# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from decimal import Decimal


class Migration(migrations.Migration):

    dependencies = [
        ('products', '0015_remove_product_retailprice'),
    ]

    operations = [
        migrations.AddField(
            model_name='product',
            name='retail_price',
            field=models.DecimalField(default=Decimal('999.000'), max_digits=20, decimal_places=3),
        ),
    ]

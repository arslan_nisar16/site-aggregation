# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('products', '0029_auto_20151211_0946'),
    ]

    operations = [
        migrations.AlterField(
            model_name='product',
            name='internalId',
            field=models.PositiveIntegerField(null=True, db_index=True),
        ),
    ]

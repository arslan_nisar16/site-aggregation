# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('products', '0031_auto_20151211_1459'),
    ]

    operations = [
        migrations.AlterField(
            model_name='product',
            name='sales_rate',
            field=models.FloatField(default=0, db_index=True),
        ),
    ]

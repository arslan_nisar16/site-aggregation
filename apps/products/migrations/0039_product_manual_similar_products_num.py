# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('products', '0038_auto_20151226_1838'),
    ]

    operations = [
        migrations.AddField(
            model_name='product',
            name='manual_similar_products_num',
            field=models.PositiveIntegerField(default=0, db_index=True),
        ),
    ]

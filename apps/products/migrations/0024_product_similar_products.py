# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('products', '0023_product_similar_products_num'),
    ]

    operations = [
        migrations.AddField(
            model_name='product',
            name='similar_products',
            field=models.ManyToManyField(related_name='_product_similar_products_+', to='products.Product'),
        ),
    ]

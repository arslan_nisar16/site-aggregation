# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from decimal import Decimal


class Migration(migrations.Migration):

    dependencies = [
        ('products', '0013_remove_metrics_price'),
    ]

    operations = [
        migrations.AddField(
            model_name='metrics',
            name='price',
            field=models.DecimalField(default=Decimal('0.000'), max_digits=20, decimal_places=3),
        ),
    ]

# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('products', '0008_auto_20151103_0701'),
    ]

    operations = [
        migrations.AddField(
            model_name='product',
            name='test1',
            field=models.PositiveIntegerField(null=True),
        ),
    ]

# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from decimal import Decimal


class Migration(migrations.Migration):

    dependencies = [
        ('products', '0016_product_retail_price'),
    ]

    operations = [
        migrations.AddField(
            model_name='product',
            name='pageviews',
            field=models.PositiveIntegerField(default=0),
        ),
        migrations.AddField(
            model_name='product',
            name='weight',
            field=models.DecimalField(default=Decimal('0.000'), max_digits=20, decimal_places=3),
        ),
    ]

# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from decimal import Decimal


class Migration(migrations.Migration):

    dependencies = [
        ('products', '0028_auto_20151210_1536'),
    ]

    operations = [
        migrations.AlterField(
            model_name='metrics',
            name='pageviews',
            field=models.PositiveIntegerField(default=0, null=True, db_index=True),
        ),
        migrations.AlterField(
            model_name='metrics',
            name='price',
            field=models.DecimalField(default=Decimal('0.000'), max_digits=20, decimal_places=3, db_index=True),
        ),
        migrations.AlterField(
            model_name='metrics',
            name='sales',
            field=models.PositiveIntegerField(default=0, null=True, db_index=True),
        ),
        migrations.AlterField(
            model_name='metrics',
            name='timestamp',
            field=models.DateTimeField(auto_now_add=True, db_index=True),
        ),
        migrations.AlterField(
            model_name='product',
            name='title',
            field=models.CharField(max_length=1000, db_index=True),
        ),
        migrations.AlterField(
            model_name='product',
            name='url',
            field=models.URLField(unique=True, max_length=1000, db_index=True),
        ),
    ]

# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('products', '0045_remove_product_selling_speed'),
    ]

    operations = [
        migrations.CreateModel(
            name='OwnProduct',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('url', models.URLField(unique=True, max_length=1000, db_index=True)),
                ('title', models.CharField(max_length=1000, db_index=True)),
                ('selling_speed', models.FloatField(default=0, db_index=True)),
                ('website', models.ForeignKey(to='products.Website')),
            ],
            options={
                'abstract': False,
            },
        ),
    ]

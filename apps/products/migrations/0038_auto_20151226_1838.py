# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('products', '0037_product_last_google_image_scraped_time'),
    ]

    operations = [
        migrations.AddField(
            model_name='product',
            name='similar_image_products',
            field=models.ManyToManyField(related_name='_product_similar_image_products_+', to='products.Product'),
        ),
        migrations.AddField(
            model_name='product',
            name='similar_image_products_num',
            field=models.PositiveIntegerField(default=0),
        ),
    ]

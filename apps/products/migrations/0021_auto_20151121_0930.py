# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


def update_weight(apps, schema_editor):
    """
    Updates all 'weight' field values in Product model. Formula is:
    weight = sales / competitorNumber
    """
    Product = apps.get_model("products", "Product")
    for product in Product.objects.all():
        metric_exists = product.metrics.exists()
        if metric_exists:
            metric = product.metrics.latest("timestamp")
            product.weight = metric.sales / metric.competitorNumber
            product.save()


def update_competitor_number(apps, schema_editor):
    """
    Updates all :model:'products.Metric' competitorNumber values to 1.
    """
    Metrics = apps.get_model("products", "Metrics")
    Metrics.objects.update(competitorNumber=1)


class Migration(migrations.Migration):

    dependencies = [
        ('products', '0020_remove_product_pageviews'),
    ]

    operations = [
        migrations.RunPython(update_competitor_number),
        migrations.RunPython(update_weight),
    ]

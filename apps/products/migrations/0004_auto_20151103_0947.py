# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('products', '0003_auto_20151103_0939'),
    ]

    operations = [
        migrations.AddField(
            model_name='metrics',
            name='score1',
            field=models.FloatField(default=0),
        ),
        migrations.AlterField(
            model_name='metrics',
            name='score',
            field=models.FloatField(default=0),
        ),
    ]

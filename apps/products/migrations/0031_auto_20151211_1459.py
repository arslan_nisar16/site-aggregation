# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from decimal import Decimal


def update_fields(apps, schema_editor):
    Product = apps.get_model("products", "Product")
    for product in Product.objects.all():
        metric = product.metrics.order_by('-timestamp').first()
        if not metric:
            continue
        product.price = metric.price
        product.sales = metric.sales
        product.pageviews = metric.pageviews
        product.save()


def reverse(*args, **kwargs):
    pass


class Migration(migrations.Migration):

    dependencies = [
        ('products', '0030_auto_20151211_1008'),
    ]

    operations = [
        migrations.AddField(
            model_name='product',
            name='pageviews',
            field=models.PositiveIntegerField(default=0, null=True, db_index=True),
        ),
        migrations.AddField(
            model_name='product',
            name='price',
            field=models.DecimalField(default=Decimal('0.000'), max_digits=20, decimal_places=3, db_index=True),
        ),
        migrations.AddField(
            model_name='product',
            name='sales',
            field=models.PositiveIntegerField(default=0, null=True, db_index=True),
        ),
        migrations.AddField(
            model_name='product',
            name='sales_rate',
            field=models.FloatField(null=True, db_index=True),
        ),
        migrations.RunPython(update_fields, reverse)
    ]

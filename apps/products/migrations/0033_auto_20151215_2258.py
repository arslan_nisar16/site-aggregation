# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from decimal import Decimal


class Migration(migrations.Migration):

    dependencies = [
        ('products', '0032_auto_20151214_1646'),
    ]

    operations = [
        migrations.AlterField(
            model_name='metrics',
            name='price',
            field=models.DecimalField(default=Decimal('0'), max_digits=20, decimal_places=0, db_index=True),
        ),
        migrations.AlterField(
            model_name='product',
            name='price',
            field=models.DecimalField(default=Decimal('0'), max_digits=20, decimal_places=0, db_index=True),
        ),
    ]

# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('products', '0047_sale_ownproduct'),
    ]

    operations = [
        migrations.AlterUniqueTogether(
            name='ownproduct',
            unique_together=set([('url', 'website')]),
        ),
    ]

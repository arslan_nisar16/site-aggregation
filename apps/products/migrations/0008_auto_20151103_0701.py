# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('products', '0007_auto_20151103_0949'),
    ]

    operations = [
        migrations.AlterField(
            model_name='shop',
            name='lastVisited',
            field=models.DateTimeField(auto_now_add=True),
        ),
        migrations.AlterField(
            model_name='shop',
            name='score',
            field=models.FloatField(default=0.0),
        ),
    ]

# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('products', '0012_auto_20151106_1607'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='metrics',
            name='price',
        ),
    ]

# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('products', '0004_auto_20151103_0947'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='metrics',
            name='score',
        ),
    ]

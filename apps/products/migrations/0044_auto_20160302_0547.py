# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('products', '0043_updated_help_text'),
    ]

    operations = [
        migrations.CreateModel(
            name='Sale',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('amount', models.PositiveIntegerField(default=0)),
                ('transaction_datetime', models.DateTimeField(db_index=True)),
                ('created', models.DateTimeField(auto_now_add=True)),
            ],
            options={
                'ordering': ('transaction_datetime',),
            },
        ),
        migrations.AddField(
            model_name='product',
            name='selling_speed',
            field=models.FloatField(default=0, db_index=True),
        ),
        migrations.AddField(
            model_name='sale',
            name='product',
            field=models.ForeignKey(to='products.Product'),
        ),
        migrations.AlterUniqueTogether(
            name='sale',
            unique_together=set([('product', 'transaction_datetime')]),
        ),
    ]

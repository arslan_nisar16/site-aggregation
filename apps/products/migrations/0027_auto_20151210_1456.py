# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import urlparse

from django.db import migrations, models


def add_existing_website(apps, schema_editor):
    Category = apps.get_model("products", "Category")
    Website = apps.get_model("products", "Website")
    websites_url = {}
    for cat in Category.objects.all():
        domain = urlparse.urlparse(cat.url).netloc
        if domain in websites_url:
            website = websites_url[domain]
        else:
            website = Website(
                    base_url=domain, home_url=domain, title=domain
                    )
            website.save()
            websites_url[domain] = website
        cat.website = website
        cat.save()


class Migration(migrations.Migration):

    dependencies = [
        ('products', '0026_auto_20151208_1633'),
    ]

    operations = [
        migrations.CreateModel(
            name='Website',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('base_url', models.CharField(unique=True, max_length=255)),
                ('home_url', models.URLField(unique=True, max_length=255)),
                ('title', models.CharField(unique=True, max_length=255)),
            ],
        ),
        migrations.AddField(
            model_name='category',
            name='website',
            field=models.ForeignKey(related_name='categories', to='products.Website', null=True),
        ),
        migrations.RunPython(add_existing_website)
    ]

# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('products', '0044_auto_20160302_0547'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='product',
            name='selling_speed',
        ),
    ]

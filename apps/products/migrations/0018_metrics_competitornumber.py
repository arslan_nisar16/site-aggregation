# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('products', '0017_auto_20151118_0147'),
    ]

    operations = [
        migrations.AddField(
            model_name='metrics',
            name='competitorNumber',
            field=models.PositiveIntegerField(default=0, null=True),
        ),
    ]

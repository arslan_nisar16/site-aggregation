# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Keywords',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('keyword', models.CharField(unique=True, max_length=255)),
                ('seedKeyword', models.CharField(max_length=255)),
                ('seedExpander', models.CharField(max_length=255)),
                ('searchVolume', models.PositiveIntegerField()),
                ('averageCpc', models.IntegerField()),
                ('competition', models.FloatField()),
                ('productNumber', models.PositiveIntegerField()),
            ],
            options={
                'db_table': 'products_keywords',
            },
        ),
        migrations.CreateModel(
            name='Metrics',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('timestamp', models.DateTimeField()),
                ('pageviews', models.PositiveIntegerField()),
                ('likes', models.PositiveIntegerField()),
                ('price', models.PositiveIntegerField()),
                ('sales', models.PositiveIntegerField()),
                ('commentNumber', models.PositiveIntegerField()),
                ('score', models.FloatField()),
            ],
            options={
                'db_table': 'products_metrics',
            },
        ),
        migrations.CreateModel(
            name='Product',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('internalId', models.PositiveIntegerField(null=True)),
                ('url', models.URLField(unique=True, max_length=255)),
                ('retailPrice', models.PositiveIntegerField(verbose_name='Retail price')),
                ('title', models.CharField(max_length=255)),
                ('category', models.CharField(default=b'dummy category', max_length=255)),
                ('description', models.CharField(default=b'dummy description', max_length=255)),
                ('imagePath', models.CharField(default=b'dummy image path', max_length=255)),
                ('similarity', models.FloatField(default=0.0)),
                ('sellingUrls', models.URLField(max_length=255, null=True)),
                ('stock', models.PositiveIntegerField(default=1, null=True)),
            ],
            options={
                'db_table': 'products_product',
                'verbose_name': 'Product',
                'verbose_name_plural': 'Products',
            },
        ),
        migrations.CreateModel(
            name='ProductStatus',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('status', models.CharField(default=b'dummy product status', max_length=255)),
            ],
            options={
                'db_table': 'products_productstatus',
            },
        ),
        migrations.CreateModel(
            name='SearchKey',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('searchKey', models.CharField(max_length=255)),
                ('subSearchKey', models.CharField(max_length=255)),
                ('product', models.ForeignKey(to='products.Product')),
            ],
            options={
                'db_table': 'products_searchkey',
            },
        ),
        migrations.CreateModel(
            name='Shop',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=255)),
                ('url', models.URLField(unique=True, max_length=255)),
                ('feedbackNumber', models.PositiveIntegerField()),
                ('score', models.FloatField()),
                ('lastVisited', models.DateTimeField()),
            ],
            options={
                'db_table': 'products_shop',
            },
        ),
        migrations.CreateModel(
            name='VisitorStatus',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('time', models.DateTimeField(auto_now_add=True)),
                ('ip', models.GenericIPAddressField()),
                ('isOrder', models.BooleanField(default=False)),
                ('orderMailId', models.CharField(max_length=32, null=True)),
                ('orderMailDate', models.DateTimeField(null=True)),
                ('product', models.ForeignKey(to='products.Product')),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.AddField(
            model_name='product',
            name='shop',
            field=models.ForeignKey(related_name='products', to='products.Shop', null=True),
        ),
        migrations.AddField(
            model_name='product',
            name='status',
            field=models.ForeignKey(to='products.ProductStatus', null=True),
        ),
        migrations.AddField(
            model_name='metrics',
            name='product',
            field=models.ForeignKey(related_name='metrics', to='products.Product'),
        ),
    ]

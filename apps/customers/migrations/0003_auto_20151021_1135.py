# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import datetime
import django_tenants.postgresql_backend.base


class Migration(migrations.Migration):

    dependencies = [
        ('customers', '0002_auto_20150618_2154'),
    ]

    operations = [
        migrations.CreateModel(
            name='UserProfile',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('activation_key', models.CharField(max_length=40, blank=True)),
                ('key_expires', models.DateTimeField(default=datetime.date(2015, 10, 21))),
            ],
            options={
                'verbose_name_plural': 'User profiles',
            },
        ),
        migrations.RemoveField(
            model_name='client',
            name='name',
        ),
        migrations.AlterField(
            model_name='client',
            name='schema_name',
            field=models.CharField(default=b'dummy_schema', unique=True, max_length=63, validators=[django_tenants.postgresql_backend.base._check_schema_name]),
        ),
        migrations.AddField(
            model_name='userprofile',
            name='user',
            field=models.OneToOneField(to='customers.Client'),
        ),
    ]

# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('customers', '0004_auto_20151023_0641'),
        ('customers', '0007_client_name'),
    ]

    operations = [
    ]

# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import django_tenants.postgresql_backend.base


class Migration(migrations.Migration):

    dependencies = [
        ('customers', '0003_auto_20151021_1253'),
    ]

    operations = [
        migrations.AlterField(
            model_name='client',
            name='schema_name',
            field=models.CharField(unique=True, max_length=63, validators=[django_tenants.postgresql_backend.base._check_schema_name]),
        ),
    ]

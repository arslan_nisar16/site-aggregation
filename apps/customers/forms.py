from django import forms
from django.contrib.sites.models import Site
from django.db import connection

from apps.customers.models import Client, Domain
from apps.users.models import User


class GenerateUsersForm(forms.Form):
  pass


class TenantForm(forms.Form):
    """
    Form for creating tenants as well as creating tenant superuser
    """
    schema_name = forms.CharField(max_length=100)
    name = forms.CharField(max_length=100)
    description = forms.CharField(widget=forms.Textarea)
    username = forms.CharField(max_length=32)
    email = forms.EmailField()
    password = forms.CharField(max_length=32, widget=forms.PasswordInput)
    confirm_password = forms.CharField(max_length=32, widget=forms.PasswordInput)

    def clean_schema_name(self):
        schema_name = self.cleaned_data.get("schema_name")
        connection.set_schema_to_public()
        try:
            tenant = Client.objects.get(schema_name=schema_name)
            raise forms.ValidationError("Tenant already exists.")
        except:
            return schema_name

    def clean(self):
        cleaned_data = super(TenantForm, self).clean()
        password = cleaned_data.get("password")
        confirm_password = cleaned_data.get("confirm_password")
        if password and confirm_password and password == confirm_password:
            return cleaned_data
        else:
            raise forms.ValidationError("Password did not match.")

    def _create_domain(self, tenant):
        tenant.deactivate()
        current_site = Site.objects.get_current()
        tenant.activate()
        domain = "%s.%s" % (tenant.schema_name, current_site.domain)
        domain, created = Domain.objects.get_or_create(domain=domain,
            tenant=tenant, is_primary=True)

    def _create_tenant(self, data):
        tenant = Client(**data)
        tenant.save()
        return tenant

    def _create_tenant_superuser(self, data):
        user = User.objects.create(username=data.get("username"), email=data.get("username"))
        user.set_password(data.get("password"))
        user.save()
        return user

    def save(self):
        data = self.cleaned_data
        tenant_data = {
            "schema_name": data.pop("schema_name"),
            "name": data.pop("name"),
            "description": data.pop("description")
        }
        tenant = self._create_tenant(tenant_data)
        self._create_domain(tenant)
        self._create_tenant_superuser(data)
        return tenant

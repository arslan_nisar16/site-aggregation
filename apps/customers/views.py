#from django.contrib.auth.models import User
from apps.users.models import User

from django.conf import settings
from django.contrib.sites.models import Site
from django.db import connection
from django.db.utils import DatabaseError
from django.views.generic import FormView, ListView
from django.views.generic.base import RedirectView
from django.views.generic.edit import CreateView

from apps.customers.forms import GenerateUsersForm, TenantForm
from apps.customers.models import Client, Domain
from random import choice


class TenantView(FormView):
  form_class = GenerateUsersForm
  template_name = "index_tenant.html"
  success_url = "/"

  def get_context_data(self, **kwargs):
    context = super(TenantView, self).get_context_data(**kwargs)
    context['tenants_list'] = Client.objects.all()
    context['users'] = User.objects.all()
    return context

  def form_valid(self, form):
    User.objects.all().delete()  # clean current users

    # generate five random users
    USERS_TO_GENERATE = 5
    first_names = ["Aiden", "Jackson", "Ethan", "Liam", "Mason", "Noah",
             "Lucas", "Jacob", "Jayden", "Jack", "Sophia", "Emma",
             "Olivia", "Isabella", "Ava", "Lily", "Zoe", "Chloe",
             "Mia", "Madison"]
    last_names = ["Smith", "Brown", "Lee	", "Wilson", "Martin", "Patel",
            "Taylor", "Wong", "Campbell", "Williams"]

    while User.objects.count() != USERS_TO_GENERATE:
      first_name = choice(first_names)
      last_name = choice(last_names)
      try:
        user = User(username=(first_name+last_name).lower(),
              email="%s@%s.com" % (first_name, last_name),
              first_name=first_name,
              last_name=last_name)
        user.save()
      except DatabaseError:
        pass

    return super(TenantView, self).form_valid(form)


class TenantIndexView(RedirectView):

    permanent = False
    url = "/products/"


class CreateTenantView(FormView):

    template_name = 'common/create.html'
    form_class = TenantForm
    success_url = '/'
    tenant = None

    def form_valid(self, form):
        tenant = form.save()
        self.tenant = tenant
        return super(CreateTenantView, self).form_valid(form)

    def get_success_url(self):
        protocol = "https" if self.request.is_secure() else "http"
        domain = self.tenant.domains.first().domain
        return "%s://%s" % (protocol, domain)

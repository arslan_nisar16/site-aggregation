# -*- coding: utf-8 -*-
from django.conf.urls import url
#from django.contrib.admin.views.decorators import staff_member_required
from django.contrib.auth.views import login, logout
from django.views.generic import TemplateView

import views

urlpatterns = [
    url(r'^signup/$', views.RegisterUser, name='signup'),
    url(r'^confirm/(?P<activation_key>\w+)/', views.register_confirm),
    url(r'^signin/$', login, name='login', kwargs={"template_name": "users/login.html"}),
    url(r'^logout_confirm/$', TemplateView.as_view(template_name='users/logout.html'), name="logout_confirm"),
    url(r'^logout/$', logout, name='logout'),
]

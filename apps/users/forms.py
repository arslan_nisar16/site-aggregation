# -*- coding: utf-8 -*-
import re

from django import forms
from django.forms.utils import ErrorList
from django.utils.translation import ugettext_lazy as _

from captcha.fields import CaptchaField

from .models import User

class UserForm(forms.ModelForm):
  password = forms.CharField(widget=forms.PasswordInput,
    label="Password")
  password1 = forms.CharField(widget=forms.PasswordInput,
    label="Confirm password")

  def clean_username(self):
    username_form = self.cleaned_data['username']
    if User.objects.filter(username=username_form).exists():
      raise forms.ValidationError(_("Username is already taken"))
    return username_form

  def clean(self):
    pwd = self.cleaned_data.get('password', "")
    pwd1 = self.cleaned_data.get('password1', "")
    if pwd != pwd1:
      if self._errors.get('password1', None) is None:
        self._errors['password1'] = ErrorList()
      self._errors['password1'].append(_("Passwords are not equal"))
    return self.cleaned_data

  def save(self):
    kwargs = self.cleaned_data.copy()
    del kwargs['password1']
    return User.objects.create_user(**kwargs)

  class Meta:
    model = User
    fields = "username", "password", "password1", "email"

from django import forms
#from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm
from models import User

class RegistrationForm(UserCreationForm):
    email = forms.EmailField(required=True, widget=forms.TextInput(attrs={'placeholder': 'E-mail address'}))
    first_name = forms.CharField(required=True)
    last_name = forms.CharField(required=True)
    captcha = CaptchaField()

    class Meta:
        model = User
        fields = ('first_name', 'last_name', 'email', 'username', 'password1', 'password2', 'captcha')

    #clean email field
    def clean_email(self):
        email = self.cleaned_data["email"]
        try:
            # Seems this cause 'relation "auth_user" does not exist' error [](http://james.lin.net.nz/2013/06/08/django-custom-user-model-in-admin-relation-auth_user-does-not-exist/)
            #User._default_manager.get(email=email)
            User.objects.get(email=email)
        except User.DoesNotExist:
            return email
        raise forms.ValidationError('duplicate email')

    def clean_password1(self):
        has_lower = False
        has_upper = False
        has_digit = False
        has_special = False
        password = self.cleaned_data.get("password1")
        if len(password) < 10:
            raise forms.ValidationError("Password must be at least 10 characters long.")

        for char in password:
            if not has_upper and char.isupper():
                    has_upper = True

            if not has_lower and char.islower():
                    has_lower = True

            if not has_digit and char.isdigit():
                    has_digit = True

            if not has_special and not re.match("^[a-zA-Z0-9]*$", char):
                has_special = True

        if not has_upper or not has_lower or not has_digit or not has_special:
            raise forms.ValidationError("Password must have at least 1 lowercase, 1 uppercase, 1 digit, and 1 special character in it.")

        return password


    #modify save() method so that we can set user.is_active to False when we first create our user
    def save(self, commit=True):    
        user = super(RegistrationForm, self).save(commit=False)
        user.email = self.cleaned_data['email']
        if commit:
            user.is_active = False # not active until he opens activation link
            user.save()

        return user

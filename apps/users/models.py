# -*- coding: utf-8 -*-
from django.contrib.auth.models import AbstractUser
from django.db import models
import datetime


class User(AbstractUser):
    USERNAME_FIELD = 'username'
    REQUIRED_FIELDS = ['email', 'award']

    award = models.IntegerField(default=0)

    def __unicode__(self):
        return self.username

    def get_rewarded(self):
        self.award += 10
        self.save()

    def GetRewarded(self):
        pass


def get_valid_thru_date():
    """ Returns date: current date + 1 day """
    return datetime.date.today() + datetime.timedelta(days=1)


class UserProfile(models.Model):
    user = models.OneToOneField(User)
    activation_key = models.CharField(max_length=40, blank=True)
    key_expires = models.DateTimeField(default=get_valid_thru_date)

    def __str__(self):
        return self.user.username

    class Meta:
        verbose_name_plural = 'User profiles'
